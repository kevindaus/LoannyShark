const mix = require('laravel-mix');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/js/app.js', 'public/js/app.js')
    .js('resources/js/gijgo.js', 'public/js/gijgo.js')
    .js('resources/js/autocomplete.js', 'public/js/autocomplete.js')
    .js('resources/js/core.js', 'public/js/core.js')
    .styles('resources/css/fonts.css','public/css/fonts.css')
    .styles('resources/css/app.css','public/css/site_style.css')
    .styles('resources/css/font-awesome.min.css','public/css/font-awesome.min.css')
    .copyDirectory('node_modules/font-awesome/fonts', 'public/fonts')
    .sass('resources/sass/material-dashboard.scss', 'public/css')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/gijgo.scss', 'public/css');
