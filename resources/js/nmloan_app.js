window.LoanForm = function () {
    var currentModule = this;
    currentModule.loan_amount = 0;
    currentModule.loan_rate = 0;
    currentModule.terms = 0;
    currentModule.amount_per_installment = 0;
    currentModule.amount_to_be_paid = 0;
    currentModule.service_charge = 0;
    currentModule.inspection_fee = 0;
    currentModule.notarial = 0;
    currentModule.other_fee = 0;
    currentModule.args = {};
    currentModule.addEventListeners = function () {
        /* add event listener for calculator */
        document.getElementById(currentModule.args.service_charge_field).addEventListener("change", function (event) {
            currentModule.service_charge = parseFloat(document.getElementById(currentModule.args.service_charge_field).value);
            currentModule.service_charge = isNaN(currentModule.service_charge) ? 0 : currentModule.service_charge;
            currentModule.recalculateTotalAmount();
        });
        document.getElementById(currentModule.args.inspection_fee_field).addEventListener("change", function (event) {
            currentModule.inspection_fee = parseFloat(document.getElementById(currentModule.args.inspection_fee_field).value);
            currentModule.inspection_fee = isNaN(currentModule.inspection_fee) ? 0 : currentModule.inspection_fee;
            currentModule.recalculateTotalAmount();
        });
        document.getElementById(currentModule.args.notarial_field).addEventListener("change", function (event) {
            currentModule.notarial = parseFloat(document.getElementById(currentModule.args.notarial_field).value);
            currentModule.notarial = isNaN(currentModule.notarial) ? 0 : currentModule.notarial;
            currentModule.recalculateTotalAmount();
        });
        document.getElementById(currentModule.args.other_fee_field).addEventListener("change", function (event) {
            currentModule.other_fee = parseFloat(document.getElementById(currentModule.args.other_fee_field).value);
            currentModule.other_fee = isNaN(currentModule.other_fee) ? 0 : currentModule.other_fee;
            currentModule.recalculateTotalAmount();
        });
        document.getElementById(currentModule.args.loan_amount_field).addEventListener("change", function (event) {
            currentModule.loan_amount = parseFloat(document.getElementById(currentModule.args.loan_amount_field).value);
            currentModule.loan_amount = isNaN(currentModule.loan_amount) ? 0 : currentModule.loan_amount;
            currentModule.recalculateTotalAmount();
        });
        document.getElementById(currentModule.args.loan_rate_field).addEventListener("change", function (event) {
            var selectedOptionIndex = document.getElementById(currentModule.args.loan_rate_field).selectedIndex;
            currentModule.loan_rate = document.getElementById(currentModule.args.loan_rate_field).options[selectedOptionIndex].dataset.percentage;
            currentModule.loan_rate = parseFloat(currentModule.loan_rate);
            currentModule.loan_rate = isNaN(currentModule.loan_rate) ? 0 : currentModule.loan_rate;
            currentModule.recalculateTotalAmount();
        });
        document.getElementById(currentModule.args.terms_field).addEventListener('change', function (event) {
            /* compute amount per installment */
            currentModule.terms = parseInt(document.getElementById(currentModule.args.terms_field).value);
            currentModule.terms = isNaN(currentModule.terms) ? 0 : currentModule.terms;
            currentModule.calculateAmountPerInstallment();
        });
    };
    currentModule.getFieldValue = function (target_field) {
        let curVal = parseFloat(document.getElementById(target_field).value);
        return isNaN(curVal) ? 0 : curVal;
    }
    currentModule.initializeValues = function () {
        currentModule.loan_amount = currentModule.getFieldValue(currentModule.args.loan_amount_field);
        currentModule.loan_rate = currentModule.getFieldValue(currentModule.args.loan_rate_field);
        currentModule.terms = currentModule.getFieldValue(currentModule.args.terms_field);
        currentModule.amount_per_installment = currentModule.getFieldValue(currentModule.args.amount_per_installment);
        currentModule.amount_to_be_paid = currentModule.getFieldValue(currentModule.args.amount_to_be_paid_field);
        currentModule.service_charge = currentModule.getFieldValue(currentModule.args.service_charge_field);
        currentModule.inspection_fee = currentModule.getFieldValue(currentModule.args.inspection_fee_field);
        currentModule.notarial = currentModule.getFieldValue(currentModule.args.notarial_field);
        currentModule.other_fee = currentModule.getFieldValue(currentModule.args.other_fee_field);
    };
    currentModule.init = function (args) {
        currentModule.args = args;
        currentModule.addEventListeners();
        currentModule.initializeValues();
    };
    currentModule.calculateAmountPerInstallment = function () {
        let args = currentModule.args;
        currentModule.amount_per_installment = Math.round(parseFloat(currentModule.loan_amount / currentModule.terms) * 100) / 100;
        currentModule.amount_per_installment = isNaN(currentModule.amount_per_installment) ? 0 : currentModule.amount_per_installment;
        document.getElementById(currentModule.args.amount_per_installment).value = currentModule.amount_per_installment;
    };
    currentModule.recalculateTotalAmount = function () {
        console.log(currentModule.loan_amount);
        console.log(currentModule.loan_rate);
        console.log(currentModule.terms);
        console.log(currentModule.amount_per_installment);
        console.log(currentModule.amount_to_be_paid);
        console.log(currentModule.service_charge);
        console.log(currentModule.inspection_fee);
        console.log(currentModule.notarial);
        console.log(currentModule.other_fee);
        currentModule.amount_to_be_paid = (currentModule.loan_amount + (currentModule.loan_amount * (currentModule.loan_rate / 100))) + (currentModule.service_charge + currentModule.inspection_fee + currentModule.notarial + currentModule.other_fee);
        document.getElementById(currentModule.args.amount_to_be_paid_field).value = currentModule.amount_to_be_paid;
        return currentModule.amount_to_be_paid;
    };
    return currentModule;
};


window.LoanPaymentForm = function (args) {
    let loanPaymentForm = this;
    loanPaymentForm.args = args;
    loanPaymentForm.init = function (args) {
        loanPaymentForm.args = args;
    };
    loanPaymentForm.clearLoanField = function () {
        jQuery('#' + loanPaymentForm.args.loan_member).find("option").remove();
    };
    loanPaymentForm.loadLoan = function (data) {

        jQuery.each(data, function (index, currentData) {
            let loanName = currentData.loan_type+ " Loan | Balance :  P " + currentData.balance + " ";
            let newOption = new Option(loanName, currentData.id);
            document.querySelector('#' + loanPaymentForm.args.loan_member).append(newOption);
            document.querySelector('#' + loanPaymentForm.args.amount_per_installment).value = currentData.amount_per_installment;

            document.querySelector('#' + loanPaymentForm.args.loan_date).value = currentData.formatted_loan_date;
            document.querySelector('#' + loanPaymentForm.args.maturity_date_field).value = currentData.formatted_maturity_date;
            document.querySelector('#' + loanPaymentForm.args.payment_due_date).value = currentData.payment_due_date.raw;

            document.querySelector('#' + loanPaymentForm.args.payment_due_date_formatted).value = currentData.payment_due_date.formatted;
            document.querySelector('#' + loanPaymentForm.args.next_payment_due_date).value = currentData.next_payment_due_date.raw;
            document.querySelector('#' + loanPaymentForm.args.next_payment_due_date_formatted).value = currentData.next_payment_due_date.formatted;

            if (currentData.next_payment_due_date.formatted == undefined) {
                document.querySelector('#' + loanPaymentForm.args.amount_per_installment).value = currentData.balance;
            }
            document.querySelector('#' + loanPaymentForm.args.remaining_balance).value = currentData.balance;
            document.querySelector('#' + loanPaymentForm.args.number_of_terms).innerHTML = currentData.terms;

        });
    };
    loanPaymentForm.loadMemberLoan = function (member_id) {
        /* retrieve the loan that is not yet paid by this member*/
        axios.get('/api/member/' + member_id + '/loans')
            .then(function (data) {
                loanPaymentForm.clearLoanField();
                loanPaymentForm.loadLoan(data.data);
            })
    };
    return loanPaymentForm;
};
jQuery(document).ready(function ($) {
    $('#maturity_date').datepicker();
    $('#release_date').datepicker();
    $('#date_loaned').datepicker();
    $('.datepicker-field').datepicker();
    $('.date_of_birth').datepicker({
        'yearRange': '-18:0'
    });

    $('[data-toggle="tooltip"]').tooltip()
});
