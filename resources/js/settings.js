$(document).ready(function () {
    window.LoanApplication = (function () {
        let current_app = this;
        current_app.default = function () {
            current_app.this_week_collection_container = '#dailySalesChart';
            current_app.this_week_collection_data = [
                [12, 17, 7, 17, 23, 18, 38]
            ];
            current_app.monthly_collection_container = '#websiteViewsChart';
            current_app.monthly_collection_data_report = [
                [100, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895]
            ];
        };
        current_app.setThisWeekCollectionData = function (data) {
            current_app.this_week_collection_data = data
        };
        current_app.setMonthlyColletionReport = function (data) {
            current_app.monthly_collection_data_report = data;
        };
        current_app.initializeThisWeekCollection = function () {
            var thisWeekCollectionChart = {
                labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                series: current_app.this_week_collection_data
            };
            var thisWeekCollectionChartSettings = {
                lineSmooth: Chartist.Interpolation.cardinal({
                    tension: 0
                }),
                low: 0,
                high: 10000, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
                chartPadding: {
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0
                },
            };
            current_app.this_week_collection_widget = new Chartist.Line(current_app.this_week_collection_container, thisWeekCollectionChart, thisWeekCollectionChartSettings);
        };
        current_app.initializeMonthlyCollection = function () {
            let monthly_collection = {
                labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                series: current_app.monthly_collection_data_report
            };
            let monthly_collection_settings = {
                axisX: {
                    showGrid: true
                },
                low: 0,
                high: 10000,
                chartPadding: {
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0
                }
            };
            let responsiveOptions = [
                ['screen and (max-width: 800px)', {
                    seriesBarDistance: 0,
                    axisX: {
                        labelInterpolationFnc: function (value) {

                            return value[0];
                        }
                    }
                }]
            ];
            current_app.this_months_collection = Chartist.Bar(current_app.monthly_collection_container, monthly_collection, monthly_collection_settings, responsiveOptions);


        };

        current_app.get_remote_data = function () {
            jQuery.get('/loan-payment/month', function (month, textStatus, xhr) {
                let monthData = [];
                jQuery.each(month, function (key, value) {
                    monthData.push({
                        'meta':value.total_payment,
                        'value':value.total_payment
                    })
                });
                console.log([monthData])
                LoanApplication.setMonthlyColletionReport([monthData]);
                LoanApplication.initializeMonthlyCollection();

            });
            jQuery.get('/loan-payment/week', function (week, textStatus, xhr) {
                let weeklyReport = Object.values(week);
                let finalReport = [];
                jQuery.each(weeklyReport, function (index , val) {
                    finalReport.push({
                        'meta': val,
                        'value': val,
                    });
                });
                //[12, 17, 7, 17, 23, 18, 38]
                console.log([weeklyReport]);
                LoanApplication.setThisWeekCollectionData([weeklyReport]);
                LoanApplication.initializeThisWeekCollection();

            });

        };
        return current_app;
    })();

});
