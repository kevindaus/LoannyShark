@extends('layouts.app', ['activePage' => 'staff', 'titlePage' => __('All members')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('layouts.widget.alert')
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">
                                {{ env('APP_NAME')  }} Staffs
                            </h4>
                            <p class="card-category">
                                List of {{ env('APP_NAME')  }} Staffs
                            </p>
                        </div>
                        <div class="card-body">
                            <div class="row mt-3">
                                <div class="col-12">
                                    <form action="{{url()->current()}}">
                                        @if($request->has('name'))
                                            Search result : {{$request->get('name')}}
                                        @endif
                                        <div class="input-group mb-3">
                                            <input autofocus type="text" class="form-control"
                                                   placeholder="Search staff (e.g John Doe)"
                                                   aria-label="Search staff (e.g John Doe)"
                                                   aria-describedby="basic-addon2" name="name">
                                            <div class="input-group-append">
                                                <button class="btn btn-sm btn-outline-secondary" type="button">
                                                    <i class="fa fa-search" style="font-size: 13px"></i>
                                                    Search
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <a href="{{ route('staff.create') }}"
                                       class="btn btn-sm btn-primary">{{ __('Add Staff') }}</a>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table mt-3">
                                    <thead class=" text-primary">
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Position
                                    </th>
                                    <th class="text-center">
                                        Action
                                    </th>
                                    </thead>
                                    <tbody>
                                    @foreach($staffs as $currentStaff)
                                        <tr>
                                            <td>
                                                {{$currentStaff->fullName()}}
                                            </td>
                                            <td>
                                                {{$currentStaff->position}}
                                            </td>
                                            <td class="td-actions text-center">
                                                <a href="{{route('staff.show',['staff'=> $currentStaff->id] )}}"
                                                   class="btn btn-default btn-link btn-sm">
                                                    <i class="fa fa-eye"></i> View
                                                </a>
                                                <a href="{{route('staff.update',['staff'=> $currentStaff->id]) }}"
                                                   class="btn btn-default btn-link btn-sm">
                                                    <i class="material-icons">edit</i> Edit
                                                </a>
                                                <form
                                                    action="{{ route('staff.destroy', ['staff'=>$currentStaff->id]) }}"
                                                    method="post" class="d-inline">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="button" class="btn btn-danger btn-link"
                                                            data-original-title="" title=""
                                                            onclick="confirm('{{ __("Are you sure you want to delete this record?") }}') ? this.parentElement.submit() : ''">
                                                        <i class="material-icons">close</i>
                                                        <div class="ripple-container"></div>
                                                        Delete
                                                    </button>
                                                </form>


                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
