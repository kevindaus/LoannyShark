@extends('layouts.app', ['activePage' => 'staff', 'titlePage' => __('All members')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">
                                {{ env('APP_NAME')  }} Staffs
                            </h4>
                            <p class="card-category">
                                List of {{ env('APP_NAME')  }} Staffs
                            </p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <a href="{{ route('staff.index') }}"
                                       class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table mt-3 table-bordered">
                                    <tbody>
                                    <tr>
                                        <td>
                                            ID
                                        </td>
                                        <td>
                                            {{$staff->id}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('First name') }}
                                        </td>
                                        <td>
                                            {{$staff->first_name}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _("Middle name") }}
                                        </td>
                                        <td>
                                            {{$staff->middle_name}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Last name') }}
                                        </td>
                                        <td>
                                            {{$staff->last_name}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Suffix') }}
                                        </td>
                                        <td>
                                            {{$staff->suffix}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Position') }}
                                        </td>
                                        <td>
                                            {{$staff->position}}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
