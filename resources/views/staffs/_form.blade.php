<form method="post" action="{{ route('staff.store') }}" autocomplete="off" class="form-horizontal">
    @csrf
    @method('post')
    @if(isset($staff->id))
        <input type="hidden" name="id" value="{{$staff->id}}">
    @endif
    <div class="card ">
        <div class="card-body ">
            <div class="row">
                <legend>Personal Information</legend>
            </div>
            <div class="row">
                <div class="col-2">
                    <div class="form-group{{ $errors->has('title') ? ' has-danger' : '' }}">
                        <label for="title" class="bmd-label-static">Title</label>
                        <input type="text" class="form-control" id="title" name="title"
                               value="{{ isset($staff->title) ? $staff->title:old('title')  }}">
                        @if ($errors->has('title'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('title') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group {{ $errors->has('first_name') ? ' has-danger' : '' }}">
                        <label for="firstname" class="bmd-label-static">Firstname</label>
                        <input type="text" class="form-control" id="firstname" name="first_name"
                               value="{{ isset($staff->first_name) ? $staff->first_name:old('first_name')  }}">

                        @if ($errors->has('first_name'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('first_name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group {{ $errors->has('middle_name') ? ' has-danger' : '' }}">
                        <label for="middle_name" class="bmd-label-static">Middlename</label>
                        <input type="text" class="form-control" id="middle_name" name="middle_name"
                               value="{{ isset($staff->middle_name) ?   $staff->middle_name:old('middle_name')  }}">
                        @if ($errors->has('middle_name'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('middle_name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group {{ $errors->has('last_name') ? ' has-danger' : '' }}">
                        <label for="last_name" class="bmd-label-static">Lastname</label>
                        <input type="text" class="form-control" id="last_name" name="last_name"
                               value="{{ isset($staff->last_name) ? $staff->last_name:old('last_name')  }}">
                        @if ($errors->has('last_name'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('last_name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group {{ $errors->has('suffix') ? ' has-danger' : '' }}">
                        <label for="suffix" class="bmd-label-static">Suffix e.g Jr / Sr</label>
                        <input type="text" class="form-control" id="suffix" name="suffix"
                               value="{{ isset($staff->suffix) ? $staff->suffix:old('suffix')  }}">
                        @if ($errors->has('suffix'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('suffix') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-12 mt-2">
                    <div class="form-group {{ $errors->has('position') ? ' has-danger' : '' }}">
                        <label for="position" class="bmd-label-static">Position</label>
                        <select name="position" class="form-control">
                            @foreach ($positions as $positionKey => $currenPosition)
                                <option value="{{$positionKey}}" {!! ($staff->position ?? old('position')) == $positionKey ? "selected='selected'":""  !!}>
                                    {{$currenPosition}}
                                </option>
                            @endforeach
                        </select>


                        @if ($errors->has('position'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('position') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group {{ $errors->has('date_of_birth') ? ' has-danger' : '' }}">
                        <label for="date_of_birth" class="bmd-label-static">Date of birth</label>
                        <input type="text" class="form-control date_of_birth" id="date_of_birth" name="date_of_birth"
                               value="{{ isset($staff->date_of_birth) ?   date("m/d/Y",strtotime($staff->date_of_birth)) :old('date_of_birth')  }}">
                        @if ($errors->has('date_of_birth'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('date_of_birth') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <legend>Account Information</legend>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-block btn-primary">
                {{ __('Submit') }}
            </button>
        </div>
    </div>
</form>
