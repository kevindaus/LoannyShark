<form action="{{route('loan.store',['loan_type'=>$loan_type])}}" method="POST" class="row" id="loanForm">
    @csrf
    @method('POST')

    @if(isset($loan))
        <input name="id" value="{{$loan->id}}">
    @endif


    <legend class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-5">
    	Loan Information
    </legend>
    <div class="col-lg-6">
        <div class="form-group{{ $errors->has('member_id') ? ' has-danger' : '' }}">
            <label for="member_id" class="bmd-label-static">Member</label>
            {{
               Form::select('member_id', $allMembers,  $loan->member_id ?? old('member_id') , ['class'=>'form-control'])
            }}
            @if ($errors->has('member_id'))
                <span id="name-error" class="error text-danger"
                      for="input-name">{{ $errors->first('member_id') }}</span>
            @endif
        </div>
    </div>
    <div class="col-lg-6">
	    <label for="loan_rate_id" class="bmd-label-static">Loan Rate </label>
	    <div class="form-group{{ $errors->has('loan_rate_id') ? ' has-danger' : '' }}">

            <select class="form-control" id="loan_rate_field" name="loan_rate_id">
                <option value="" selected="selected">-- Select Loan Rate --</option>
                @foreach($loan_rates as $currentLoanRate)
                    <option value="{{$currentLoanRate->id}}" data-percentage="{{$currentLoanRate->interest_rate}}" {{ ($loan->loan_rate_id ?? old('loan_rate_id')) == $currentLoanRate->id ? "selected='selected'":""  }}>
                        {{$currentLoanRate->name}}
                    </option>
                @endforeach
            </select>


	        @if ($errors->has('loan_rate_id'))
	            <span id="name-error" class="error text-danger"
	                  for="input-name">{{ $errors->first('loan_rate_id') }}</span>
	        @endif
	    </div>
    </div>

    <div class="col-6 mt-4">
        <div class="form-group {{ $errors->has('date_loaned') ? ' has-danger' : '' }}">
            <label for="date_loaned" class="bmd-label-static">Date Loaned</label>
            <input autocomplete="off" type="text" class="form-control date_loaned"  id="date_loaned" name="date_loaned"  value="{{ isset($loan->date_loaned) ?  $loan->date_loaned->format("M d,y")  :old('date_loaned')  }}">
            @if ($errors->has('date_loaned'))
                <span id="name-error" class="error text-danger"
                      for="input-name">{{ $errors->first('date_loaned') }}</span>
            @endif
        </div>
    </div>

    <div class="col-6  mt-4 " style="display: none">
        <div class="form-group {{ $errors->has('loan_type') ? ' has-danger' : '' }}">
            <label for="loan_type" class="bmd-label-static">Loan Type</label>
            <input type="text" class="form-control" id="loan_type" name="loan_type" value="{{ $loan->loan_type ?? $loan_type  }}" readonly="readonly">

            @if ($errors->has('loan_type'))
                <span id="name-error" class="error text-danger"
                      for="input-name">{{ $errors->first('loan_type') }}</span>
            @endif
        </div>
    </div>
     <div class="col-12 mt-5">
        <div class="form-group {{ $errors->has('amount_loaned') ? ' has-danger' : '' }}">
            <label for="amount_loaned" class="bmd-label-static">Loan Amount</label>
            <input type="number" class="form-control" id="amount_loaned" name="amount_loaned" value="{{ $loan->amount_loaned ??   old('amount_loaned')  }}">
            @if ($errors->has('amount_loaned'))
                <span id="name-error" class="error text-danger"
                      for="input-name">{{ $errors->first('amount_loaned') }}</span>
            @endif
        </div>
    </div>

    <div class="col-12  mt-4">
        <div class="form-group {{ $errors->has('maturity_date') ? ' has-danger' : '' }}">
            <label for="maturity_date" class="bmd-label-static">Maturity Date</label>
            <input  autocomplete="off" type="text" class="form-control maturity_date" id="maturity_date" name="maturity_date"  value="{{ isset($loan->maturity_date) ?   $loan->maturity_date->format("M d,y") :old('maturity_date')  }}">
            @if ($errors->has('maturity_date'))
                <span id="name-error" class="error text-danger"
                      for="input-name">{{ $errors->first('maturity_date') }}</span>
            @endif
        </div>
    </div>

    <div class="col-6  mt-4">
        <div class="form-group {{ $errors->has('terms') ? ' has-danger' : '' }}">
            <label for="terms" class="bmd-label-static">Terms (months)</label>
            <input type="number" class="form-control" id="terms" name="terms" value="{{ $loan->terms ??   old('terms')  }}">
            @if ($errors->has('terms'))
                <span id="name-error" class="error text-danger"
                      for="input-name">{{ $errors->first('terms') }}</span>
            @endif
        </div>
    </div>
	<div class="col-6  mt-4">
	    <div class="form-group {{ $errors->has('amount_per_installment') ? ' has-danger' : '' }}">
	        <label for="amount_per_installment" class="bmd-label-static"> Amount per installment</label>
	        <input step="0.01" type="number" class="form-control" id="amount_per_installment" name="amount_per_installment" value="{{ $loan->amount_per_installment ??   old('amount_per_installment')  }}">
	        @if ($errors->has('amount_per_installment'))
	            <span id="name-error" class="error text-danger"
	                  for="input-name">{{ $errors->first('amount_per_installment') }}</span>
	        @endif
	    </div>
	</div>
	<div class="col-12  mt-4">
	    <div class="form-group {{ $errors->has('release_date') ? ' has-danger' : '' }}">
	        <label for="release_date" class="bmd-label-static">Release Date</label>
	        <input  autocomplete="off" type="text" class="form-control release_date datepicker" id="release_date" name="release_date"  value="{{ isset($loan->release_date) ?   $loan->release_date->format("M d,y"):old('release_date')  }}">
	        @if ($errors->has('release_date'))
	            <span id="name-error" class="error text-danger"
	                  for="input-name">{{ $errors->first('release_date') }}</span>
	        @endif
	    </div>
	</div>
    <legend class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-5">
    	Staff
    </legend>
	<div class="col-4  mt-4">
	    <label for="prepared_by" class="bmd-label-static">Prepared By</label>
	    <div class="form-group{{ $errors->has('prepared_by') ? ' has-danger' : '' }}">
	        {{
	           Form::select('prepared_by',$staff_list , $loan->prepared_by ?? old('prepared_by') , ['class'=>'form-control'])
	        }}
	        @if ($errors->has('prepared_by'))
	            <span id="name-error" class="error text-danger"
	                  for="input-name">{{ $errors->first('prepared_by') }}</span>
	        @endif
	    </div>
	</div>

	<div class="col-4  mt-4">
	    <label for="checked_by" class="bmd-label-static">Checked By</label>
	    <div class="form-group{{ $errors->has('checked_by') ? ' has-danger' : '' }}">
	        {{
	           Form::select('checked_by', $staff_list, $loan->checked_by ?? old('checked_by') , ['class'=>'form-control'])
	        }}
	        @if ($errors->has('checked_by'))
	            <span id="name-error" class="error text-danger"
	                  for="input-name">{{ $errors->first('checked_by') }}</span>
	        @endif
	    </div>
	</div>
	<div class="col-4  mt-4">
	    <label for="approved_by" class="bmd-label-static">Approved By</label>
	    <div class="form-group{{ $errors->has('approved_by') ? ' has-danger' : '' }}">
	        {{
	           Form::select('approved_by', $staff_list, $loan->approved_by ?? old('approved_by') , ['class'=>'form-control'])
	        }}
	        @if ($errors->has('approved_by'))
	            <span id="name-error" class="error text-danger"
	                  for="input-name">{{ $errors->first('approved_by') }}</span>
	        @endif
	    </div>
	</div>
    <legend class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-5">
    	Fee
    </legend>
	<div class="col-4  mt-4">
	    <div class="form-group {{ $errors->has('service_charge') ? ' has-danger' : '' }}">
	        <label for="service_charge" class="bmd-label-static">Service Charge</label>
	        <input type="number" class="form-control" id="service_charge" name="service_charge" value="{{ $loan->service_charge ??   old('service_charge')  }}">
	        @if ($errors->has('service_charge'))
	            <span id="name-error" class="error text-danger"
	                  for="input-name">{{ $errors->first('service_charge') }}</span>
	        @endif
	    </div>
	</div>

	<div class="col-4  mt-4">
	    <div class="form-group {{ $errors->has('inspection_fee') ? ' has-danger' : '' }}">
	        <label for="inspection_fee" class="bmd-label-static">Inspection Fee</label>
	        <input type="number" class="form-control" id="inspection_fee" name="inspection_fee" value="{{ $loan->inspection_fee ??   old('inspection_fee')  }}">
	        @if ($errors->has('inspection_fee'))
	            <span id="name-error" class="error text-danger"
	                  for="input-name">{{ $errors->first('inspection_fee') }}</span>
	        @endif
	    </div>
	</div>
	<div class="col-4  mt-4">
	    <div class="form-group {{ $errors->has('notarial') ? ' has-danger' : '' }}">
	        <label for="notarial" class="bmd-label-static">Notarial</label>
	        <input type="number" class="form-control" id="notarial" name="notarial" value="{{ $loan->notarial ??   old('notarial')  }}">
	        @if ($errors->has('notarial'))
	            <span id="name-error" class="error text-danger"
	                  for="input-name">{{ $errors->first('notarial') }}</span>
	        @endif
	    </div>
	</div>
	<div class="col-4  mt-4">
	    <div class="form-group {{ $errors->has('other') ? ' has-danger' : '' }}">
	        <label for="other" class="bmd-label-static">Other fee</label>
	        <input type="number" class="form-control" id="other" name="other" value="{{ $loan->other ??   old('other')  }}">
	        @if ($errors->has('other'))
	            <span id="name-error" class="error text-danger"
	                  for="input-name">{{ $errors->first('other') }}</span>
	        @endif
	    </div>
	</div>
    <legend class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-5">
    	Optional Fields
    </legend>
	<div class="col-4  mt-4">
	    <div class="form-group {{ $errors->has('insurance') ? ' has-danger' : '' }}">
	        <label for="insurance" class="bmd-label-static">Insurance</label>
	        <input type="number" class="form-control" id="insurance" name="insurance" value="{{ $loan->insurance ??   old('insurance')  }}">
	        @if ($errors->has('insurance'))
	            <span id="name-error" class="error text-danger"
	                  for="input-name">{{ $errors->first('insurance') }}</span>
	        @endif
	    </div>
	</div>

	<div class="col-4  mt-4">
	    <div class="form-group {{ $errors->has('beneficiary') ? ' has-danger' : '' }}">
	        <label for="beneficiary" class="bmd-label-static">beneficiary</label>
	        <input type="number" class="form-control" id="beneficiary" name="beneficiary" value="{{ $loan->beneficiary ??   old('beneficiary')  }}">
	        @if ($errors->has('beneficiary'))
	            <span id="name-error" class="error text-danger"
	                  for="input-name">{{ $errors->first('beneficiary') }}</span>
	        @endif
	    </div>
	</div>
	<div class="col-4  mt-4">
	    <div class="form-group {{ $errors->has('share_capital') ? ' has-danger' : '' }}">
	        <label for="share_capital" class="bmd-label-static">Share Capital</label>
	        <input type="number" class="form-control" id="share_capital" name="share_capital" value="{{ $loan->share_capital ??   old('share_capital')  }}">
	        @if ($errors->has('share_capital'))
	            <span id="name-error" class="error text-danger"
	                  for="input-name">{{ $errors->first('share_capital') }}</span>
	        @endif
	    </div>
	</div>

    <div class="col-12 mt-4">
        <div class="form-group {{ $errors->has('amount_to_be_paid') ? ' has-danger' : '' }}">
            <label for="amount_to_be_paid" class="bmd-label-static">Amount to be paid (calculated)</label>
            <input readonly="readonly" type="number" class="form-control" id="amount_to_be_paid" name="amount_to_be_paid" ">
            @if ($errors->has('amount_to_be_paid'))
                <span id="name-error" class="error text-danger"
                      for="input-name">{{ $errors->first('amount_to_be_paid') }}</span>
            @endif
        </div>
    </div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<br>
        <button type="submit" class="btn btn-block btn-primary">
            {{ __('Submit') }}
        </button>
	</div>
</form>
@push('js')
    <script type="text/javascript">
        var form = new LoanForm();
        let config = {
            service_charge_field: "service_charge",
            inspection_fee_field: "inspection_fee",
            other_fee_field: "other",
            notarial_field: "notarial",
            loan_rate_field: "loan_rate_field",
            loan_amount_field: "amount_loaned",
            amount_per_installment: "amount_per_installment",
            terms_field: "terms",
            amount_to_be_paid_field: "amount_to_be_paid"
        };
        form.init(config);
        form.calculateAmountPerInstallment(config);
        form.recalculateTotalAmount();


    </script>

@endpush

