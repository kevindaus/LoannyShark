@extends('layouts.app', ['activePage' => 'loan', 'titlePage' => __('All Loans')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">
                                {{ env('APP_NAME')  }} Loan Record
                            </h4>
                            <p class="card-category">
                                List of all loan record
                            </p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 text-right">
                                    <a href="{{ route('member.register') }}"
                                       class="btn btn-sm btn-primary">{{ __('New Loan') }}</a>
                                </div>
                            </div>
                            @TODO - Add confirmation form
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
