@extends('layouts.app', ['activePage' => 'loan', 'titlePage' => __('All Loans')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">
                                {{ env('APP_NAME')  }} Loan Record
                            </h4>
                            <p class="card-category">
                                List of all loan record
                            </p>
                        </div>
                        <div class="card-body">
                            <div class="row mt-3">
                                <div class="col-12">
                                    <form action="{{url()->current()}}">
                                        @if($request->has('name'))
                                            Search result : {{$request->get('name')}}
                                        @endif
                                        <div class="input-group mb-3">
                                            <input autofocus type="text" class="form-control" placeholder="Search member (e.g John Doe)" aria-label="Search member (e.g John Doe)" aria-describedby="basic-addon2" name="name">
                                            <div class="input-group-append">
                                                <button class="btn btn-sm btn-outline-secondary" type="button">
                                                    <i class="fa fa-search" style="font-size: 13px"></i>
                                                    Search
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                @can('make_loan',auth()->user(),\App\Loan::class)
                                <div class="float-left">
                                    <div class="dropdown">
                                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                                            Make a Loan
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ route('loan.register',['loan_type'=>'agricultural']) }}" class="">{{ __('Agricultural Loan') }}</a></li>
                                            <li><a href="{{ route('loan.register',['loan_type'=>'contingency']) }}" class="">{{ __('Contingency Loan') }}</a></li>
                                            <li><a href="{{ route('loan.register',['loan_type'=>'commodity']) }}" class="">{{ __('Rice / Commodity Loan') }}</a></li>
                                            <li><a href="{{ route('loan.register',['loan_type'=>'multipurpose']) }}" class="">{{ __('Multi Purpose Loan') }}</a></li>
                                        </ul>
                                    </div>
                                </div>
                                @endcan
                                @can('view_pending_loan',\App\Loan::class)
                                <div class="float-right ml-3">
                                    <a href="{{ route('loan.unapproved') }}"
                                       class="btn btn-sm btn-default">{{ __('Pending Approval Loan') }}
                                    </a>
                                </div>
                                @endcan
                            </div>
                            <div class="table-responsive">
                                <table class="table mt-3">
                                    <thead class=" text-primary">
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Contact #
                                    </th>
                                    <th>
                                        Loan
                                    </th>
                                    <th>
                                        Remaining Balance
                                    </th>
                                    <th>
                                        Maturity Date
                                    </th>
                                    <th class="text-center">
                                        Action
                                    </th>
                                    </thead>
                                    <tbody>
                                    @foreach($loans as $currentLoan)
                                        <tr>
                                            <td>
                                                <a href="{{route('member.view',['member'=>$currentLoan->member->id])}}">
                                                {{ sprintf("%s %s %s" , $currentLoan->member->title ,$currentLoan->member->first_name , $currentLoan->member->last_name ) }}
                                                </a>
                                            </td>
                                            <td>
                                                {{$currentLoan->member->mobile_number}}
                                            </td>
                                            <td class="text-info text-center">
                                                {{ @peso($currentLoan->amount_loaned) }}
                                            </td>
                                            <td class="text-info text-center">
                                                {{ @peso($currentLoan->balance) }}
                                            </td>
                                            <td class="text-warning">
                                                {{ $currentLoan->maturity_date->format("M d,Y")  }}
                                            </td>
                                            <td class="td-actions text-center" >
                                                @can('view_loan',$currentLoan)
                                                <a href="{{route('loan.view',['loan_type'=>strtolower($currentLoan->loan_type),'loan'=>$currentLoan])}}" class="btn btn-default btn-link btn-sm">
                                                    <i class="fa fa-eye"></i> View
                                                </a>
                                                @endcan
                                                @can('update_loan',$currentLoan)
                                                <a href="{{route('loan.update',['loan_type'=>strtolower($currentLoan->loan_type),'loan'=>$currentLoan->id])}}" class="btn btn-default btn-link btn-sm">
                                                    <i class="material-icons">edit</i> Edit
                                                </a>
                                                @endcan
                                                @can('print_loan',$currentLoan)
                                                <a href="{{route('loan.print',['loan_type'=>strtolower($currentLoan->loan_type),'loan'=>$currentLoan->id])}}" class="btn btn-default btn-link btn-sm">
                                                    <i class="fa fa-print"></i> Print
                                                </a>
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="6">
                                            {{ $loans->links() }}
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
