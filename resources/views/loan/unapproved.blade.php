@extends('layouts.app', ['activePage' => 'loan', 'titlePage' => __('All Loans')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('layouts.widget.alert')
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">
                                {{ env('APP_NAME')  }} Loan Record
                            </h4>
                            <p class="card-category">
                                List of all loan record
                            </p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-6 text-left">
                                    <a href="{{ route('loan.index') }}"
                                       class="btn btn-sm btn-primary">{{ __('Back to list') }}
                                    </a>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table mt-3">
                                    <thead class=" text-primary">
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Contact #
                                    </th>
                                    <th>
                                        Loan
                                    </th>
                                    <th>
                                        Remaining Balance
                                    </th>
                                    <th>
                                        Maturity Date
                                    </th>
                                    <th class="text-center">
                                        Action
                                    </th>
                                    </thead>
                                    <tbody>
                                    @foreach($loans as $currentLoan)
                                        <tr>
                                            <td>
                                                <a href="{{route('member.view',['member'=>$currentLoan->member->id])}}">
                                                    {{ sprintf("%s %s %s" , $currentLoan->member->title ,$currentLoan->member->first_name , $currentLoan->member->last_name ) }}
                                                </a>
                                            </td>
                                            <td>
                                                {{$currentLoan->member->mobile_number}}
                                            </td>
                                            <td class="text-info text-center">
                                                {{ @peso($currentLoan->amount_loaned) }}
                                            </td>
                                            <td class="text-info text-center">
                                                {{ @peso($currentLoan->balance ) }}
                                            </td>
                                            <td class="text-warning">
                                                {{ $currentLoan->maturity_date->format("M d,Y")  }}
                                            </td>
                                            <td class="td-actions text-center" >
                                                <a href="{{route('loan.approve',['loan'=>$currentLoan->id])}}" class="btn btn-default btn-link btn-sm">
                                                    <i class="fa fa-check"></i> Approve
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="6">
                                            {{ $loans->links() }}
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
