@extends('layouts.app', ['activePage' => 'loan', 'titlePage' => __('All Loans')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('layouts.widget.alert')
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">
                                {{ env('APP_NAME')  }} Loan Record
                            </h4>
                            <p class="card-category">
                                List of all loan record
                            </p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                @can('view_all_loans',\App\Loan::class)
                                    <div class="col-6">
                                        <a href="{{ route('loan.index') }}"
                                           class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                                    </div>
                                @endcan
                                @can('print_loan',auth()->user(),$loan)
                                    <div class="col-6 text-right">
                                        <a href="{{ route('loan.print', [
                                    'loan_type'=> strtolower($loan->loan_type),
                                    'loan' => $loan->id
                                    ]) }}"
                                           class="btn btn-sm btn-primary">
                                            <i class="fa fa-print"></i> {{ __('PRINT') }}
                                        </a>
                                    </div>
                                @endcan
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <legend class="text-center mt-5">
                                        Loan Information
                                    </legend>
                                    <table class="table table-bordered table-hover mt-3" style="">
                                        <tbody>
                                        <tr>
                                            <td><strong><label for="member_id"
                                                               class="bmd-label-static">Member</label></strong></td>
                                            <td>{{ sprintf("%s %s %s",$loan->member->title , $loan->member->first_name , $loan->member->last_name) }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong><label for="loan_rate_id" class="bmd-label-static">Loan
                                                        Rate </label></strong></td>
                                            <td>{{ $loan->loan_rate->name }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong><label for="date_loaned" class="bmd-label-static">Date
                                                        Loaned</label></strong></td>
                                            <td>{{$loan->date_loaned->format("M d,y")}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong><label for="loan_type" class="bmd-label-static">Loan
                                                        Type</label></strong></td>
                                            <td>{{$loan->loan_type}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong><label for="amount_loaned" class="bmd-label-static">Loan
                                                        Amount</label></strong></td>
                                            <td>{{ @peso($loan->amount_loaned) }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong><label for="maturity_date" class="bmd-label-static">Maturity
                                                        Date</label></strong></td>
                                            <td>{{$loan->maturity_date->format("M d,Y")}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong><label for="terms"
                                                               class="bmd-label-static">Terms</label></strong></td>
                                            <td>{{$loan->terms}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong><label for="amount_per_installment"
                                                               class="bmd-label-static">
                                                        Amount per installment</strong></td>
                                            <td>{{ @peso($loan->amount_per_installment)  }}</td>
                                        </tr>
                                        <tr>
                                            <td><label for="release_date" class="bmd-label-static">Release
                                                    Date</label>
                                            </td>
                                            <td>{{$loan->release_date->format("M d,y")}}</td>
                                        </tr>
                                        <tr>
                                            <td><label for="prepared_by" class="bmd-label-static">Prepared
                                                    By</label>
                                            </td>
                                            <td>
                                                {{ $loan->prepared_by()->first()->fullName()}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for="checked_by" class="bmd-label-static">Checked By</label>
                                            </td>
                                            <td>{{$loan->checked_by()->first()->fullName() }}</td>
                                        </tr>
                                        <tr>
                                            <td><label for="approved_by" class="bmd-label-static">Approved
                                                    By</label>
                                            </td>
                                            <td>
                                                {{$loan->approved_by()->first()->fullName() }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label for="service_charge" class="bmd-label-static">Service
                                                    Charge</label></td>
                                            <td>{{@peso($loan->service_charge)}}</td>
                                        </tr>
                                        <tr>
                                            <td><label for="inspection_fee" class="bmd-label-static">Inspection
                                                    Fee</label></td>
                                            <td>{{@peso($loan->inspection_fee)}}</td>
                                        </tr>
                                        <tr>
                                            <td><label for="notarial" class="bmd-label-static">Notarial</label></td>
                                            <td>{{@peso($loan->notarial)}}</td>
                                        </tr>
                                        <tr>
                                            <td><label for="other" class="bmd-label-static">Other fee</label></td>
                                            <td>{{@peso($loan->other)}}</td>
                                        </tr>
                                        <tr>
                                            <td><label for="insurance" class="bmd-label-static">Insurance</label>
                                            </td>
                                            <td>{{$loan->insurance}}</td>
                                        </tr>

                                        <tr>
                                            <td><label for="beneficiary"
                                                       class="bmd-label-static">Beneficiary</label>
                                            </td>
                                            <td>{{$loan->beneficiary}}</td>
                                        </tr>
                                        <tr>
                                            <td><label for="share_capital" class="bmd-label-static">Share
                                                    Capital</label></td>
                                            <td>{{$loan->share_capital}}</td>
                                        </tr>
                                        <tr>
                                            <td><label for="amount_to_be_paid" class="bmd-label-static">Total
                                                    Computed</label></td>
                                            <td>{{@peso($loan->amount_to_be_paid)}}</td>
                                        </tr>
                                        <tr>
                                            <td><label for="amount_to_be_paid" class="bmd-label-static">Balance</label>
                                            </td>
                                            <td>{{@peso($loan->balance)}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-6">
                                    <legend class="text-center mt-5">
                                        Loan Payments
                                    </legend>
                                    <table class="table table-bordered table-hover mt-3">
                                        <tbody>
                                        <thead>
                                        <tr>
                                            <td>#</td>
                                            <td>Amount</td>
                                            <td>Payment Date</td>
                                            <td>Due Date</td>
                                        </tr>
                                        </thead>
                                        <tr>
                                            @foreach($loan->loan_payments as $current_payment)
                                                <td>{{$current_payment->id}}</td>
                                                <td>{{@peso($current_payment->amount)}}</td>
                                                <td>{{$current_payment->payment_date->format("F d,Y")}}</td>
                                                <td>{{$current_payment->due_date->format("F d,Y")}}</td>
                                            @endforeach
                                        </tr>
                                        </tbody>
                                    </table>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
