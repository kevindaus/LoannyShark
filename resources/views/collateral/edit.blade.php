@extends('layouts.app', ['activePage' => 'collateral', 'titlePage' => __('Make loan payment')])
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-info" role="alert">
                            {{ session()->get('success')  }}
                        </div>
                    @endif

                    <div class="card ">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">{{ __('Collateral') }}</h4>
                            <p class="card-category"></p>
                        </div>
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-12">
                                    <a href="{{ route('collateral.all') }}"
                                       class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                                </div>
                            </div>
                            <br>
                            <form method="post" action="{{ route('collateral.update',['collateral'=>$collateral->id]) }}" autocomplete="off" class="form-horizontal row">
                                @include('collateral._form', ['members'=>$members,'collateral'=>$collateral] )
                            </form>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
