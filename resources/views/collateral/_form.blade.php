    @csrf
    @method('post')
    @if ($errors->any())
        <div class="col-12">
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Error:</strong>
                <br>
                @if ($errors->any())
                    {!! implode('', $errors->all('<div>:message</div>')) !!}
                @endif
            </div>
        </div>
    @endif
    <div class="col-12">
        <label class="">Owner</label>
        <select class="form-control" id="member_id" name="member_id">
            <option value="" selected="selected">-- Member --</option>
            @foreach($members as $current_member)
                <option
                    value="{{$current_member->id}}" {!! ($collateral->member->id ?? old('member_id' ,$collateral->member->id ?? '')) == $current_member->id ? "selected='selected'":""  !!}>
                    {{ sprintf("%s %s %s",$current_member->title , $current_member->first_name , $current_member->last_name) }}
                </option>
            @endforeach
        </select>
        <label class="mt-5">Name (e.g Honda TMX 150 , Toyotta Corrola 1990)</label>
        <div class="form-group {{ $errors->has('name') ? ' has-danger' : '' }}">
            <input type="text" class="form-control" id="name" name="name"
                   value="{{ old('name' , ($collateral->name ?? '') ) }}">

            @if ($errors->has('name'))
                <span id="name-error" class="error text-danger"
                      for="input-name">{{ $errors->first('name') }}</span>
            @endif
        </div>
        <label class="mt-5">Type (e.g Motorcycle , Land , Car)</label>
        <div class="form-group {{ $errors->has('type') ? ' has-danger' : '' }}">
            <input type="text" class="form-control" id="type" name="type"
                   value="{{ old('type' , $collateral->type ?? '') }}">
            @if ($errors->has('type'))
                <span id="name-error" class="error text-danger"
                      for="input-name">{{ $errors->first('name') }}</span>
            @endif
        </div>
        <label class="mt-5">Description (e.g Red TMX 150 with SideCar)</label>

        {{old('description' , $collateral->description ?? '')}}
        <div class="form-group {{ $errors->has('description') ? ' has-danger' : '' }}">
            <textarea class="form-control" name="description" id="description" cols="30" rows="10">{{old('description' , $collateral->description ?? '')}}</textarea>
            @if ($errors->has('description'))
                <span id="name-error" class="error text-danger"
                      for="input-name">{{ $errors->first('description') }}</span>
            @endif
        </div>

    </div>

    <div class="col-12">
        <button type="submit" class="btn btn-block btn-primary">
            {{ __('Save') }}
        </button>
    </div>

