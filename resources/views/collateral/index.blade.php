@extends('layouts.app', ['activePage' => 'collateral', 'titlePage' => __('All collaterals')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('layouts.widget.alert')
                    <div class="card">

                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">
                                {{ env('APP_NAME')  }} Collaterals
                            </h4>
                            <p class="card-category">
                                List of {{ env('APP_NAME')  }} Collaterals of members
                            </p>
                        </div>
                        <div class="card-body">
                            <div class="row mt-3">
                                <div class="col-12">
                                    <form action="{{url()->current()}}">
                                        @if($request->has('name'))
                                            Search result : {{$request->get('name')}}
                                        @endif
                                        <div class="input-group mb-3">
                                            <input autofocus type="text" class="form-control" placeholder="Search member (e.g John Doe)" aria-label="Search member (e.g John Doe)" aria-describedby="basic-addon2" name="name">
                                            <div class="input-group-append">
                                                <button class="btn btn-sm btn-outline-secondary" type="button">
                                                    <i class="fa fa-search" style="font-size: 13px"></i>
                                                    Search
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 text-left">
                                    <a href="{{ route('collateral.add') }}"
                                       class="btn btn-sm btn-primary">{{ __('Register new collateral') }}
                                    </a>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table mt-3">
                                    <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Owner
                                    </th>
                                    <th>
                                        Collateral
                                    </th>
                                    <th>
                                        Description
                                    </th>
                                    <th class="text-center">
                                        Action
                                    </th>
                                    </thead>
                                    <tbody>
                                    @foreach($collaterals as $current_collateral)
                                        <tr>
                                            <td>
                                                {{ $current_collateral->id  }}
                                            </td>
                                            <td>
                                                {{ sprintf(
                                                    "%s %s %s",
                                                    $current_collateral->member->title ,
                                                    $current_collateral->member->first_name,
                                                    $current_collateral->member->last_name)
                                                }}
                                            </td>
                                            <td>
                                                {{ $current_collateral->type }}
                                            </td>
                                            <td>
                                                {{ $current_collateral->description }}
                                            </td>
                                            <td class="td-actions text-center" style="width: 300px">
                                                <a href="{{route('collateral.edit',['member'=>$current_collateral->id])}}" class="btn btn-default btn-link btn-sm">
                                                    <i class="material-icons">update</i> Edit
                                                </a>
                                                <form action="{{ route('collateral.destroy', ['collateral'=>$current_collateral->id]) }}" method="post" class="d-inline">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('{{ __("Are you sure you want to delete this record?") }}') ? this.parentElement.submit() : ''">
                                                        <i class="material-icons">close</i>
                                                        <div class="ripple-container"></div>
                                                        Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="5">
                                            {{ $collaterals->links() }}
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
