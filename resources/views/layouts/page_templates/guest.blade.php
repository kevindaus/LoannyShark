@include('layouts.navbars.navs.guest')
<div class="wrapper wrapper-full-page" data-background-image="">
  <div class="page-header login-page header-filter" filter-color="black"  data-color="green" style="background: #565656">
  <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
    @yield('content')
    @include('layouts.footers.guest')
  </div>
</div>
