<div class="sidebar" data-color="green" data-background-color="white">
    <div class="logo">
        <a href="{{route('dashboard.index')}}" class="simple-text logo-normal">
            {{ env('APP_NAME_SHORT')  }}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">

            @can('view_dashboard',auth()->user())
            <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('dashboard.index') }}">
                    <i class="material-icons">dashboard</i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>
            @endcan
            @can('view_member',auth()->user())
            <li class="nav-item{{ $activePage == 'member' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('member.index') }}">
                    <i class="fa fa-users"></i>
                    <p>{{ __('Members') }}</p>
                </a>
            </li>
            @endcan
            @can('view_all_loans',\App\Loan::class)
                <li class="nav-item{{ $activePage == 'loan' ? ' active' : '' }}">
                    <a class="nav-link" href="{{ route('loan.index') }}">
                        <i class="material-icons">library_books</i>
                        <p>{{ __('Loan') }}</p>
                    </a>
                </li>

            @endcan
            @can('view_collateral',auth()->user())
            <li class="nav-item{{ $activePage == 'collateral' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('collateral.all') }}">
                    <i class="material-icons">assessment</i>
                    <p>{{ __('Collateral') }}</p>
                </a>
            </li>
            @endcan
            @can('view_payment_history',auth()->user())
            <li class="nav-item{{ $activePage == 'loan_payment' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('loan_payment.index') }}">
                    <i class="fa fa-dollar"></i>
                    <p>{{ __('Loan Payment') }}</p>
                </a>
            </li>
            @endcan
            @can('view_users',auth()->user())
            <li class="nav-item {{ ($activePage == 'user' || $activePage == 'user-management') ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('user.index') }}">
                    <i class="fa fa-lock"></i>
                    <span class="sidebar-normal"> {{ __('User Account') }} </span>
                </a>
            </li>
            @endcan
            @can('view_staff',auth()->user())
            <li class="nav-item {{ ($activePage == 'staff' ) ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('staff.index') }}">
                    <i class="fa fa-building"></i>
                    <span class="sidebar-normal"> {{ __('Staff') }} </span>
                </a>
            </li>
            @endcan
            @can('view_settings',auth()->user())
            <li class="nav-item{{ $activePage == 'settings' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('dashboard.settings') }}">
                    <i class="fa fa-cogs"></i>
                    <p>{{ __('Settings') }}</p>
                </a>
            </li>
            @endcan
            @can('release_loan',auth()->user())
                <li class="nav-item{{ $activePage == 'loan' ? ' active' : '' }}">
                    <a class="nav-link" href="{{ route('loan_payment.create') }}">
                        <i class="fa fa-user"></i>
                        <p>{{ __('Make Payment') }}</p>
                    </a>
                </li>
                <li class="nav-item{{ $activePage == 'loan' ? ' active' : '' }}">
                    <a class="nav-link" href="{{ route('home') }}">
                        <i class="fa fa-download"></i>
                        <p>{{ __('Release Loan') }}</p>
                    </a>
                </li>

            @endcan
        </ul>
    </div>
</div>
