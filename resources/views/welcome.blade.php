@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'home', 'title' => __('Material Dashboard')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="container" style="height: auto;">
                <div class="row justify-content-center">
                    <div class="col-lg-7 col-md-8">
                        <h1 class="text-white text-center">{{ __('Welcome to .').env('APP_NAME') }}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
