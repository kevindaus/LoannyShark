<form method="post" action="{{ route('member.store') }}" autocomplete="off" class="form-horizontal">
    @csrf
    @method('post')
    @if(isset($member->id))
        <input type="hidden" name="id" value="{{$member->id}}" >
    @endif
    <div class="card ">
        <div class="card-header card-header-primary">
            <h4 class="card-title">{{ __('Member Information') }}</h4>
            <p class="card-category"></p>
        </div>
        <div class="card-body ">
            <div class="row">
                <div class="col-6 text-left ml-2">
                    <a href="{{ route('member.index') }}"
                       class="btn btn-sm btn-primary">{{ __('Back to list') }}
                    </a>
                </div>
                <div class="col-6 text-right">
                </div>
            </div>
            <legend class="mt-4">Personal Information</legend>
            <div class="row pt-4">
                <div class="col-2">
                    <div class="form-group{{ $errors->has('title') ? ' has-danger' : '' }}">
                        {{
                        Form::select('title', [
                            '' => '-- Title --',
                            'Ms.' => 'Ms.',
                            'Mr.' => 'Mr.',
                            'Mrs.' => 'Mrs.',
                            'Prof.' => 'Prof.',
                            'Dr.' => 'Dr.',
                            ], isset($member->title) ? $member->title:old('title') , ['class'=>'form-control','id'=>'title'])
                        }}
                        @if ($errors->has('title'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('title') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group {{ $errors->has('first_name') ? ' has-danger' : '' }}">
                        <label for="firstname" class="bmd-label-static">Firstname</label>
                        <input type="text" class="form-control" id="firstname" name="first_name" value="{{ isset($member->first_name) ? $member->first_name:old('first_name')  }}">

                        @if ($errors->has('first_name'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('first_name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group {{ $errors->has('middle_name') ? ' has-danger' : '' }}">
                        <label for="middle_name" class="bmd-label-static">Middlename</label>
                        <input type="text" class="form-control" id="middle_name" name="middle_name"  value="{{ isset($member->middle_name) ?   $member->middle_name:old('middle_name')  }}">
                        @if ($errors->has('middle_name'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('middle_name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group {{ $errors->has('last_name') ? ' has-danger' : '' }}">
                        <label for="last_name" class="bmd-label-static">Lastname</label>
                        <input type="text" class="form-control" id="last_name" name="last_name" value="{{ isset($member->last_name) ? $member->last_name:old('last_name')  }}">
                        @if ($errors->has('last_name'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('last_name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group {{ $errors->has('suffix') ? ' has-danger' : '' }}">
                        <label for="suffix" class="bmd-label-static">Suffix e.g Jr / Sr</label>
                        <input type="text" class="form-control" id="suffix" name="suffix"  value="{{ isset($member->suffix) ? $member->suffix:old('suffix')  }}">
                        @if ($errors->has('suffix'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('suffix') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-12 mt-2">
                    <div class="form-group {{ $errors->has('religion') ? ' has-danger' : '' }}">
                        <label for="religion" class="bmd-label-static">Religion (Roman Catholic / Iglesia ni
                            Kristo)</label>
                        <input type="text" class="form-control" id="religion" name="religion" value="{{ isset($member->religion) ? $member->religion:old('religion')  }}">
                        @if ($errors->has('religion'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('religion') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group {{ $errors->has('date_of_birth') ? ' has-danger' : '' }}">
                        <label for="date_of_birth" class="bmd-label-static">Date of birth</label>
                        <input type="text" class="form-control date_of_birth" id="date_of_birth" name="date_of_birth"  value="{{ isset($member->date_of_birth) ?   date("m/d/Y",strtotime($member->date_of_birth)) :old('date_of_birth')  }}">
                        @if ($errors->has('date_of_birth'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('date_of_birth') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group{{ $errors->has('civil_status') ? ' has-danger' : '' }}">
                        {{
                           Form::select('civil_status', [
                               '' => '-- Civil Status --',
                               'single' => 'Single',
                               'married' => 'Married',
                               'widowed' => 'Widowed',
                               'widower' => 'Widower',
                               ], $member ? $member->civil_status:old('civil_status') , ['class'=>'form-control'])
                        }}
                        @if ($errors->has('civil_status'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('civil_status') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group {{ $errors->has('birthplace') ? ' has-danger' : '' }}">
                        <label for="birthplace" class="bmd-label-static">Birthplace</label>
                        <input type="text" class="form-control" id="birthplace" name="birthplace" value="{{ isset($member->birthplace) ?  $member->birthplace:old('birthplace')  }}">
                        @if ($errors->has('birthplace'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('birthplace') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group {{ $errors->has('number_of_dependents') ? ' has-danger' : '' }}">
                        <label for="number_of_dependents" class="bmd-label-static">Number of Dependents (leave empty if
                            none)</label>
                        <input type="number" class="form-control" id="number_of_dependents" name="number_of_dependents" value="{{ isset($member->number_of_dependents) ?   $member->number_of_dependents:old('number_of_dependents')  }}">
                        @if ($errors->has('number_of_dependents'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('number_of_dependents') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <legend class="mt-5">Occupation</legend>
            <div class="row pt-4">
                <div class="col-4">
                    <div class="form-group {{ $errors->has('occupation') ? ' has-danger' : '' }}">
                        <label for="occupation" class="bmd-label-static">Occupation</label>
                        <input type="text" class="form-control" id="occupation" name="occupation" value="{{ isset($member->occupation) ?   $member->occupation:old('occupation')  }}">
                        @if ($errors->has('occupation'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('occupation') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group {{ $errors->has('monthly_income') ? ' has-danger' : '' }}">
                        <label for="monthly_income" class="bmd-label-static">Monthly Income</label>
                        <input type="text" class="form-control" id="monthly_income" name="monthly_income" value="{{ isset($member->monthly_income) ?   $member->monthly_income:old('monthly_income')  }}">
                        @if ($errors->has('monthly_income'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('monthly_income') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group {{ $errors->has('other_source_of_income') ? ' has-danger' : '' }}">
                        <label for="other_source_of_income" class="bmd-label-static">Other source of income</label>
                        <input type="text" class="form-control" id="other_source_of_income" name="other_source_of_income" value="{{ isset($member->other_source_of_income) ?   $member->other_source_of_income:old('other_source_of_income')  }}">
                        @if ($errors->has('other_source_of_income'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('other_source_of_income') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <legend class="mt-5">Spouse (optional)</legend>
            <div class="row pt-4">
                <div class="col-12">
                    <div class="form-group {{ $errors->has('spouse_name') ? ' has-danger' : '' }}">
                        <label for="spouse_name" class="bmd-label-static">Spouse name</label>
                        <input type="text" class="form-control" id="spouse_name" name="spouse_name"  value="{{ isset($member->spouse_name) ?   $member->spouse_name:old('spouse_name')  }}">
                        @if ($errors->has('spouse_name'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('spouse_name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group {{ $errors->has('spouse_address') ? ' has-danger' : '' }}">
                        <label for="spouse_address" class="bmd-label-static">Spouse address</label>
                        <input type="text" class="form-control" id="spouse_address" name="spouse_address" value="{{ isset($member->spouse_address) ?   $member->spouse_address:old('spouse_address')  }}">
                        @if ($errors->has('spouse_address'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('spouse_address') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group {{ $errors->has('spouse_occupation') ? ' has-danger' : '' }}">
                        <label for="spouse_occupation" class="bmd-label-static">Spouse occupation</label>
                        <input type="text" class="form-control" id="spouse_occupation" name="spouse_occupation" value="{{ isset($member->spouse_occupation) ?   $member->spouse_occupation:old('spouse_occupation')  }}">
                        @if ($errors->has('spouse_occupation'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('spouse_occupation') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group {{ $errors->has('spouse_monthly_income') ? ' has-danger' : '' }}">
                        <label for="spouse_monthly_income" class="bmd-label-static">Spouse monthly income</label>
                        <input type="text" class="form-control" id="spouse_monthly_income" name="spouse_monthly_income" value="{{ isset($member->spouse_monthly_income) ?   $member->spouse_monthly_income:old('spouse_monthly_income')  }}">
                        @if ($errors->has('spouse_monthly_income'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('spouse_monthly_income') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <legend class="mt-5">School (optional)</legend>
            <div class="row pt-4">
                <div class="col-12">
                    <div class="form-group {{ $errors->has('elementary_school_graduated') ? ' has-danger' : '' }}">
                        <label for="elementary_school_graduated" class="bmd-label-static">Elementary School
                            Graduated</label>
                        <input type="text" class="form-control" id="elementary_school_graduated"
                               name="elementary_school_graduated" value="{{ isset($member->elementary_school_graduated) ?   $member->elementary_school_graduated:old('elementary_school_graduated')  }}">
                        @if ($errors->has('elementary_school_graduated'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('elementary_school_graduated') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group {{ $errors->has('highschool_graduated') ? ' has-danger' : '' }}">
                        <label for="highschool_graduated" class="bmd-label-static">Highschool Graduated</label>
                        <input type="text" class="form-control" id="highschool_graduated" name="highschool_graduated"  value="{{ isset($member->highschool_graduated) ?   $member->highschool_graduated:old('highschool_graduated')  }}">
                        @if ($errors->has('highschool_graduated'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('highschool_graduated') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group {{ $errors->has('college_graduated') ? ' has-danger' : '' }}">
                        <label for="college_graduated" class="bmd-label-static">College Graduated</label>
                        <input type="text" class="form-control" id="college_graduated" name="college_graduated"  value="{{ isset($member->college_graduated) ?   $member->college_graduated:old('college_graduated')  }}">
                        @if ($errors->has('college_graduated'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('college_graduated') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <legend class="mt-5">Address and Contact Information</legend>
            <div class="row pt-4">
                <div class="col-12">
                    <div class="form-group {{ $errors->has('address') ? ' has-danger' : '' }}">
                        <label for="address" class="bmd-label-static">Address (required)</label>
                        <input type="text" class="form-control" id="address" name="address"  value="{{ isset($member->address) ?   $member->address:old('address')  }}">
                        @if ($errors->has('address'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('address') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group {{ $errors->has('business_address') ? ' has-danger' : '' }}">
                        <label for="business_address" class="bmd-label-static">Business address</label>
                        <input type="text" class="form-control" id="business_address" name="business_address"  value="{{ isset($member->business_address) ?   $member->business_address:old('business_address')  }}">
                        @if ($errors->has('business_address'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('business_address') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group {{ $errors->has('telephone_number') ? ' has-danger' : '' }}">
                        <label for="telephone_number" class="bmd-label-static">Telephone number</label>
                        <input type="text" class="form-control" id="telephone_number" name="telephone_number"  value="{{ isset($member->telephone_number) ?   $member->telephone_number:old('telephone_number')  }}">
                        @if ($errors->has('telephone_number'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('telephone_number') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group {{ $errors->has('mobile_number') ? ' has-danger' : '' }}">
                        <label for="mobile_number" class="bmd-label-static">Mobile number</label>
                        <input type="text" class="form-control" id="mobile_number" name="mobile_number" value="{{ isset($member->mobile_number) ?   $member->mobile_number:old('mobile_number')  }}">
                        @if ($errors->has('mobile_number'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('mobile_number') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <legend class="mt-5">Other Information</legend>
            <div class="row pt-4">
                <div class="col-12">
                    <div class="form-group {{ $errors->has('height') ? ' has-danger' : '' }}">
                        <label for="height" class="bmd-label-static">Height</label>
                        <input type="text" class="form-control" id="height" name="height" value="{{ isset($member->height) ?   $member->height:old('height')  }}">
                        @if ($errors->has('height'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('height') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-12">
                    <div class="form-group {{ $errors->has('weight') ? ' has-danger' : '' }}">
                        <label for="weight" class="bmd-label-static">Weight</label>
                        <input type="text" class="form-control" id="weight" name="weight" value="{{ isset($member->weight) ?   $member->weight:old('weight')  }}">
                        @if ($errors->has('weight'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('weight') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-12">
                    <div class="form-group {{ $errors->has('branch') ? ' has-danger' : '' }}">
                        <label for="branch" class="bmd-label-static">Branch</label>
                        <input type="text" class="form-control" id="branch" name="branch" value="{{env("DEFAULT_BRANCH")}}" readonly >
                        @if ($errors->has('branch'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('branch') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group {{ $errors->has('date_joined') ? ' has-danger' : '' }}">
                        <label for="date_joined" class="bmd-label-static">Date joined (required)</label>
                        <input type="text" class="form-control datepicker-field" id="date_joined" name="date_joined"  value="{{ isset($member->date_joined) ? date("m/d/Y",strtotime($member->date_joined)):old('date_joined')  }}">

                        @if ($errors->has('date_joined'))
                            <span id="name-error" class="error text-danger"
                                  for="input-name">{{ $errors->first('date_joined') }}</span>
                        @endif
                    </div>
                </div>
            </div>

        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-block btn-primary">
                {{ __('Submit') }}
            </button>
        </div>
    </div>
</form>
