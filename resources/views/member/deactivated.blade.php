@extends('layouts.app', ['activePage' => 'member', 'titlePage' => __('All members')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('layouts.widget.alert')
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">
                                {{ env('APP_NAME')  }} Members
                            </h4>
                            <p class="card-category">
                                List of {{ env('APP_NAME')  }} Members
                            </p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-6 text-left">
                                    <a href="{{ route('member.index') }}"
                                       class="btn btn-sm btn-primary">{{ __('Back to list') }}
                                    </a>
                                </div>
                                <div class="col-6 text-right">
                                    <a href="{{ route('member.deactivated_members') }}"
                                       class="btn btn-sm btn-default">{{ __('View deactivated members') }}
                                    </a>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table mt-3">
                                    <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Country
                                    </th>
                                    <th>
                                        Contact #
                                    </th>
                                    <th class="text-center">
                                        Action
                                    </th>
                                    </thead>
                                    <tbody>
                                    @foreach($members as $currentMember)
                                        <tr>
                                            <td>
                                                {{ $currentMember->id  }}
                                            </td>
                                            <td>
                                                {{ sprintf("%s %s %s",$currentMember->title , $currentMember->first_name,$currentMember->last_name)  }}
                                            </td>
                                            <td>
                                                {{ $currentMember->address }}
                                            </td>
                                            <td>
                                                {{ $currentMember->mobile_number }}
                                            </td>
                                            <td class="td-actions text-center" style="width: 300px">
                                                <a href="{{route('member.reactivate',['member'=>$currentMember->id])}}" class="btn btn-default btn-link btn-sm">
                                                    <i class="fa fa-refresh"></i> Approve
                                                </a>
                                                <form action="{{route('member.delete',['member'=>$currentMember->id ] )}}" method="POST" class="d-inline">
                                                    @csrf
                                                    <button class="btn btn-default btn-link btn-sm" type="submit">
                                                        <i class="material-icons">close</i> Delete
                                                    </button>
                                                </form>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="5">
                                            {{ $members->links() }}
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
