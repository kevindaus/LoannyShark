@extends('layouts.app', ['activePage' => 'member', 'titlePage' => __('All members')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">
                                {{ env('APP_NAME')  }} Member
                            </h4>
                            <p class="card-category">
                                {{ env('APP_NAME')  }} Member Detailed Information
                            </p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <a href="{{ route('member.index') }}"
                                       class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                                </div>
                            </div>
                            <div class="row mt-5">
                                <div class="col-6">
                                    <legend>Member Information</legend>
                                <table class="table mt-3 table-bordered">
                                    <tbody>
                                    <tr>
                                        <td>
                                            ID
                                        </td>
                                        <td>
                                            {{$member->id}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('First name') }}
                                        </td>
                                        <td>
                                            {{$member->first_name}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _("Middle name") }}
                                        </td>
                                        <td>
                                            {{$member->middle_name}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Last name') }}
                                        </td>
                                        <td>
                                            {{$member->last_name}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Suffix') }}
                                        </td>
                                        <td>
                                            {{$member->suffix}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Religion') }}
                                        </td>
                                        <td>
                                            {{$member->religion}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Date of birth') }}
                                        </td>
                                        <td>
                                            {{$member->date_of_birth->format("M d,Y")}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Occupation') }}
                                        </td>
                                        <td>
                                            {{$member->occupation}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Monthly Income') }}
                                        </td>
                                        <td>
                                            {{ @peso($member->monthly_income) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Source of Income') }}
                                        </td>
                                        <td>
                                            {{$member->other_source_of_income}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Telephone number') }}
                                        </td>
                                        <td>
                                            {{$member->telephone_number}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Mobile number') }}
                                        </td>
                                        <td>
                                            {{$member->mobile_number}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Civil Status') }}
                                        </td>
                                        <td>
                                            {{$member->civil_status}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Spouse name') }}
                                        </td>
                                        <td>
                                            {{$member->spouse_name}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Address') }}
                                        </td>
                                        <td>
                                            {{$member->spouse_address}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Spouse Occupation') }}
                                        </td>
                                        <td>
                                            {{$member->spouse_occupation}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Monthly Income') }}
                                        </td>
                                        <td>
                                            {{$member->spouse_monthly_income}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Elementary School Graduated') }}
                                        </td>
                                        <td>
                                            {{$member->elementary_school_graduated}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Highschool Graduated') }}
                                        </td>
                                        <td>
                                            {{$member->highschool_graduated}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('College Graduated') }}
                                        </td>
                                        <td>
                                            {{$member->college_graduated}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Height') }}
                                        </td>
                                        <td>
                                            {{$member->height}}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            {{ _('Birthplace') }}
                                        </td>
                                        <td>
                                            {{$member->birthplace}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Number of dependents') }}
                                        </td>
                                        <td>
                                            {{$member->number_of_dependents}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Address') }}
                                        </td>
                                        <td>
                                            {{$member->address}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Business Address') }}
                                        </td>
                                        <td>
                                            {{$member->business_address}}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            {{ _('Branch') }}
                                        </td>
                                        <td>
                                            {{$member->branch}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Date Joined') }}
                                        </td>
                                        <td>
                                            {{$member->date_joined->format("M d,Y")}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{ _('Weight') }}
                                        </td>
                                        <td>
                                            {{$member->weight}}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                </div>
                                <div class="col-6">
                                    <legend>Collaterals</legend>
                                    <table class="table mt-3 table-bordered" >
                                        <thead>
                                        <tr>
                                            <td>Name</td>
                                            <td>Type</td>
                                        </tr>
                                        </thead>
                                        @foreach($member->collateral as $current_collateral)
                                        <tr>
                                            <td>
                                                <a href="{{route('collateral.show',['collateral'=>$current_collateral->id])}}" >
                                                    {{$current_collateral->name}}
                                                </a>
                                            </td>
                                            <td>
                                                {{$current_collateral->type}}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </table>
                                    <legend>Payments</legend>
                                    <table class="table mt-3 table-bordered" >
                                        <thead>
                                        <tr>
                                            <td>#</td>
                                            <td>Amount</td>
                                            <td>Payment Date</td>
                                            <td>Due Date</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            @foreach($member->loan as $current_loan)
                                                @foreach($current_loan->loan_payments as $current_payment)
                                                    <td>{{$current_payment->id}}</td>
                                                    <td>{{@peso($current_payment->amount)}}</td>
                                                    <td>{{$current_payment->payment_date->format("F d,Y")}}</td>
                                                    <td>{{$current_payment->due_date->format("F d,Y")}}</td>
                                                @endforeach
                                            @endforeach
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
