@extends('layouts.app', ['activePage' => 'member', 'titlePage' => __('Add new member')])
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('layouts.widget.alert')
                    @include('member._form')
                </div>
            </div>
        </div>
    </div>
@endsection
