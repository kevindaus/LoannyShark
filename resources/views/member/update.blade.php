@extends('layouts.app', ['activePage' => 'member', 'titlePage' => __('Update member information')])
@php
@endphp

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('layouts.widget.alert')
                    @include('member._form',['member'=>$member])
                </div>
            </div>
        </div>
    </div>
@endsection
