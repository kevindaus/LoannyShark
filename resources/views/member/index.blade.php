@extends('layouts.app', ['activePage' => 'member', 'titlePage' => __('All members')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('layouts.widget.alert')
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">
                                {{ env('APP_NAME')  }} Members
                            </h4>
                            <p class="card-category">
                                List of {{ env('APP_NAME')  }} Members
                            </p>
                        </div>
                        <div class="card-body">
                            <div class="row mt-3">
                                <div class="col-12">
                                    <form action="{{url()->current()}}">
                                    @if($request->has('name'))
                                        Search result : {{$request->get('name')}}
                                    @endif
                                    <div class="input-group mb-3">
                                        <input autofocus type="text" class="form-control" placeholder="Search member (e.g John Doe)" aria-label="Search member (e.g John Doe)" aria-describedby="basic-addon2" name="name">
                                        <div class="input-group-append">
                                            <button class="btn btn-sm btn-outline-secondary" type="button">
                                                <i class="fa fa-search" style="font-size: 13px"></i>
                                                Search
                                            </button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-6 text-left">
                                    <a href="{{ route('member.register') }}"
                                       class="btn btn-sm btn-primary">{{ __('Add member') }}
                                    </a>
                                </div>
                                <div class="col-6 text-right">
                                    <a href="{{ route('member.deactivated_members') }}"
                                       class="btn btn-sm btn-default">{{ __('View deactivated members') }}
                                    </a>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table mt-3">
                                    <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Country
                                    </th>
                                    <th>
                                        Contact #
                                    </th>
                                    <th class="text-center">
                                        Action
                                    </th>
                                    </thead>
                                    <tbody>
                                    @foreach($members as $currentMember)


                                        <tr>
                                            <td>
                                                {{ $currentMember->id  }}
                                            </td>
                                            <td>
                                                {{ sprintf("%s %s %s",$currentMember->title , $currentMember->first_name,$currentMember->last_name)  }}
                                            </td>
                                            <td>
                                                {{ $currentMember->address }}
                                            </td>
                                            <td>
                                                {{ $currentMember->mobile_number }}
                                            </td>
                                            <td class="td-actions text-center" style="width: 300px">
                                                <a href="{{route('member.view',['member'=>$currentMember->id])}}" class="btn btn-default btn-link btn-sm">
                                                    <i class="fa fa-eye"></i> View
                                                </a>
                                                <a href="{{route('member.update',['member'=>$currentMember->id])}}" class="btn btn-default btn-link btn-sm">
                                                    <i class="material-icons">refresh</i> Edit
                                                </a>
                                                <a href="{{route('member.deactivate',['member'=>$currentMember->id])}}" class="btn btn-default btn-link btn-sm">
                                                    <i class="material-icons">close</i> Deactivate
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="5">
                                            {{ $members->links() }}
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
