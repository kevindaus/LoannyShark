@extends('layouts.app', ['activePage' => 'member', 'titlePage' => __('All members')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-danger">
                            <h4 class="card-title ">
                                {{ __('Deactivating this member will update this members status to inactive. ')  }}
                            </h4>
                            <p class="card-category">

                            </p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-6 text-left ml-2">
                                    <a href="{{ route('member.index') }}"
                                       class="btn btn-sm btn-primary">{{ __('Back to list') }}
                                    </a>
                                </div>
                                <div class="col-6 text-right">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-5">
                                    <form action="{{route('member.deactivate' , ['member'=>$member])}}" method="POST">
                                        @csrf
                                        <div class="alert " role="alert">
                                            <h4>
                                                {{ __('Are you sure yop want to deactivate this user ?')  }}
                                            </h4>
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-sm btn-outline-danger">
                                                    Confirm
                                                </button>
                                                <a class="btn btn-sm btn-outline"
                                                   href="{{ route('member.index') }}">
                                                    Cancel
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-3"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
