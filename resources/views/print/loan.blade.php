<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>

<div style="text-align: center">
    <h4>
        NAGANALISAN MULTIPURPOSE COOPERATIVE <br>
        <i>Nangalisan , Bagabag , Nueva Vizcaya</i>
    </h4>

</div>
<div style="text-align: right">
    <strong>{{Carbon\Carbon::now()->format("F d, Y")}}</strong>
</div>
<div style="text-align: center">
    <h3>{{strtoupper($loan->loan_type)}} LOAN</h3>
</div>
<div class="float: left;">
    <table>
        <tr>
            <td><i>Name of borrower</i></td>
            <td> : {{$loan->member->getFullName()}} </td>
        </tr>
        <tr>
            <td><i>Beneficiary</i></td>
            <td> : {{$loan->beneficiary}}</td>
        </tr>
        <tr>
            <td><i>Address</i></td>
            <td> : {{$loan->member->address}}</td>
        </tr>
        <tr>
            <td><i>Terms (Months)</i></td>
            <td> : {{$loan->terms}}</td>
        </tr>
    </table>
</div>
<div style="float: right;">

    <table>
        <tr>
            <td>Due Date</td>
            <td> : {{$loan->maturity_date->format("F d.Y")}}</td>
        </tr>
        <tr>
            <td>Amount of Loan</td>
            <td> : {{@peso($loan->amount_loaned)}}</td>
        </tr>
    </table>
</div>
<div style="clear: both;"></div>

<div style="clear: both"></div>
<br>
<div style="float: right">
    <table style="width: 50%">
        <tr>
            <td>INTEREST ( {{$loan->loan_rate->name}} )</td>
            <td>{{  $loan->amount_loaned   + ($loan->amount_loaned * ( floatval($loan->loan_rate->interest_rate)  / 100  ))   }}</td>
        </tr>

        <tr>
            <td><i>Service Charge : </i></td>
            <td>
                <div>{{$loan->service_charge}}</div>
            </td>
        </tr>
        <tr>
            <td><i>Share Capital : </i></td>
            <td>
                <div>{{$loan->service_charge}}</div>
            </td>
        </tr>
        <tr>
            <td><i>Inspection Fee : </i></td>
            <td>
                <div>{{$loan->service_charge}}</div>
            </td>
        </tr>
        <tr>
            <td><i>Notarial : </i></td>
            <td>
                <div>{{$loan->service_charge}}</div>
            </td>
        </tr>
        <tr>
            <td><i>Others : </i></td>
            <td>
                <div>{{$loan->service_charge}}</div>
            </td>
        </tr>
        <tr>
            <td><i>Savings Deposit : </i></td>
            <td>
                <div>{{$loan->service_charge}}</div>
            </td>
        </tr>
        <tr>
            <td><i>Insurance : </i></td>
            <td>
                <div>{{$loan->service_charge}}</div>
            </td>
        </tr>
    </table>
</div>
<div style="clear: both"></div>
<div>
    <h3>NET PROCEEDS OF LOAN</h3>
</div>
<div>
    <div>
        PREPARED BY : {{$loan->prepared_by}}
    </div>
    <div>
        CHECKED BY : {{$loan->checked_by}}
    </div>
    <div>
        APPROVED BY : {{$loan->approved_by}}
    </div>
</div>

<div>
    <div>
        Loan Officer : {{$loan->prepared_by}}
    </div>
    <div>
        Bookkeeper : {{$loan->prepared_by}}
    </div>
    <div>
        Manager : {{$loan->prepared_by}}
    </div>
</div>
<div>
    Received Cash Disbursement Voucher No : _________ for ________________________ ONE TWO THREE | 12356 covering the
    proceeds of the above loan .
</div>
<div style="float: right">
    ________________
    Borrower
</div>
<div>
    <h4>Released by : </h4>
    JOHN DOE
    <i>Cashier / Treasurer</i>
</div>

</body>
</html>
