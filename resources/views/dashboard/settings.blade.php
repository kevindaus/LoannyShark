@extends('layouts.app', ['activePage' => 'settings', 'titlePage' => __('Application Settings')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('layouts.widget.alert')

                    <div class="card">
                        <div class="card-header card-header-info">
                            <h4 class="card-title ">
                                {{ env('APP_NAME')  }} Application Settings
                            </h4>
                            <p class="card-category">
                                Update application settings
                            </p>
                        </div>
                        <div class="card-body mt-3">
                            <form action="{{route('dashboard.update_settings')}}" method="POST">
                                @csrf

                                <div class="form-group mb-3">
                                    <label for="exampleFormControlInput1">Cashier</label>
                                    {{
                                    Form::select('cashier', $all_staffs, $cashier->id ?? old('cashier') , ['class'=>'form-control','id'=>'cashier'])
                                    }}
                                    @if ($errors->has('cashier'))
                                        <span id="cashier-error" class="error text-danger"
                                              for="input-name">{{ $errors->first('cashier') }}</span>
                                    @endif
                                </div>

                                <button type="submit" class="btn btn-info btn-lg btn-block">Save</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
