@extends('layouts.app', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="col-12">
                @include('layouts.widget.alert')
            </div>
            @can('view_dashboard',auth()->user())
                <div class="row">
                    <div class="col-4">
                        <div class="dropdown">
                            <button class="btn btn-lg btn-block btn-info dropdown-toggle" type="button"
                                    data-toggle="dropdown">
                                Make a Loan
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('loan.register',['loan_type'=>'agricultural']) }}"
                                       class="">{{ __('Agricultural Loan') }}</a></li>
                                <li><a href="{{ route('loan.register',['loan_type'=>'contingency']) }}"
                                       class="">{{ __('Contingency Loan') }}</a></li>
                                <li><a href="{{ route('loan.register',['loan_type'=>'commodity']) }}"
                                       class=" ">{{ __('Rice / Commodity Loan') }}</a></li>
                                <li><a href="{{ route('loan.register',['loan_type'=>'multipurpose']) }}"
                                       class=" ">{{ __('Multi Purpose Loan') }}</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-4">
                        <a class="btn btn-lg btn-default btn-block text-center" href="{{route('loan_payment.create')}}">
                            Pay Loan
                        </a>
                    </div>
                </div>
            @endcan
        </div>
        <hr>
        <div class="container-fluid">

            @can('release_loan',auth()->user())
                {{--here--}}

                <div class="row">
                    <div class="col-12">
                        <div class="card card-chart">
                            <div class="card-header card-header-success">
                                Print Loan
                            </div>
                            <div class="card-body">
                                <form action="{{url()->current()}}">
                                    @if(old('name'))
                                        Search result : {{ old('name')   }}
                                    @endif
                                    <div class="input-group mb-3">
                                        <input autofocus type="text" class="form-control"
                                               placeholder="Search member (e.g John Doe)"
                                               aria-label="Search member (e.g John Doe)" aria-describedby="basic-addon2"
                                               name="name">
                                        <div class="input-group-append">
                                            <button class="btn btn-sm btn-outline-secondary" type="submit">
                                                <i class="fa fa-search" style="font-size: 13px"></i>
                                                Search
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <div class="table-responsive">
                                    <table class="table mt-3">
                                        <thead class=" text-primary">
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Contact #
                                        </th>
                                        <th>
                                            Loan
                                        </th>
                                        <th>
                                            Remaining Balance
                                        </th>
                                        <th>
                                            Maturity Date
                                        </th>
                                        <th class="text-center">
                                            Action
                                        </th>
                                        </thead>
                                        <tbody>
                                        @foreach($loans as $currentLoan)
                                            <tr>
                                                <td>
                                                    <a href="{{route('member.view',['member'=>$currentLoan->member->id])}}">
                                                        {{ sprintf("%s %s %s" , $currentLoan->member->title ,$currentLoan->member->first_name , $currentLoan->member->last_name ) }}
                                                    </a>
                                                </td>
                                                <td>
                                                    {{$currentLoan->member->mobile_number}}
                                                </td>
                                                <td class="text-info text-center">
                                                    {{ @peso($currentLoan->amount_loaned) }}
                                                </td>
                                                <td class="text-info text-center">
                                                    {{ @peso($currentLoan->balance) }}
                                                </td>
                                                <td class="text-warning">
                                                    {{ $currentLoan->maturity_date->format("M d,Y")  }}
                                                </td>
                                                <td class="td-actions text-center">
                                                    @can('view_loan',$currentLoan)
                                                    <a href="{{route('loan.view',['loan_type'=>strtolower($currentLoan->loan_type),'loan'=>$currentLoan])}}"
                                                       class="btn btn-default btn-link btn-sm">
                                                        <i class="fa fa-eye"></i> View
                                                    </a>
                                                    @endcan
                                                    @can('update_loan',$currentLoan)
                                                    <a href="{{route('loan.update',['loan_type'=>strtolower($currentLoan->loan_type),'loan'=>$currentLoan->id])}}"
                                                       class="btn btn-default btn-link btn-sm">
                                                        <i class="material-icons">edit</i> Edit
                                                    </a>
                                                    @endcan
                                                    @can('print_loan',$currentLoan)
                                                    <a href="{{route('loan.print',['loan_type'=>strtolower($currentLoan->loan_type),'loan'=>$currentLoan->id])}}"
                                                       class="btn btn-default btn-link btn-sm">
                                                        <i class="fa fa-print"></i> Print
                                                    </a>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="6">
                                                {{ $loans->links() }}
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer" style="display: none">
                            </div>
                        </div>

                    </div>
                </div>


            @endcan
        </div>
        <div class="container-fluid">
            {{--@TODO Nice to have features--}}
            @can('view_dashboard',auth()->user())
                <div class="row" style="display: none">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">store</i>
                                </div>
                                <p class="card-category ">Recent Payment</p>
                                <h3 class="card-title"> 34</h3>
                            </div>
                            <div class="card-footer ">
                                <div class="stats">
                                    <a href="#">
                                        <i class="fa fa-chevron-circle-right"></i>
                                        View Payment History
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header card-header-info card-header-icon">
                                <div class="card-icon">
                                    <i class="fa fa-info"></i>
                                </div>
                                <p class="card-category">Near overdue</p>
                                <h3 class="card-title"> 5</h3>
                            </div>
                            <div class="card-footer">
                                <div class="stats">
                                    <a href="#">
                                        <i class="fa fa-chevron-circle-right"></i> Payment Near Overdue
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header card-header-danger card-header-icon">
                                <div class="card-icon">
                                    <i class="fa fa-warning"></i>
                                </div>
                                <p class="card-category">Overdue Payment</p>
                                <h3 class="card-title"> 2</h3>
                            </div>
                            <div class="card-footer">
                                <div class="stats">
                                    <a href="#">
                                        <i class="fa fa-chevron-circle-right"></i>
                                        View Overdue Payment
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="card card-chart">
                            <div class="card-header card-header-success">
                                <div class="ct-chart" id="thisWeekCollectionChartContainer"></div>
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">This Week's Collection </h4>
                                <p class="card-category"> Total collection per day </p>
                            </div>
                            <div class="card-footer" style="display: none">
                                <div class="stats">
                                    <a href="#">
                                        <i class="fa fa-chevron-circle-right"></i>
                                        View this week's payments
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card card-chart">
                            <div class="card-header card-header-warning">
                                <div class="ct-chart" id="monthlyCollectionChart"></div>
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">Monthly Collection Report</h4>
                                <p class="card-category"> Total Collection per Month </p>
                            </div>
                            {{-- @TODO - nice to have features --}}
                            <div class="card-footer" style="display: none">
                                <div class="stats">
                                    <a href="#">
                                        <i class="fa fa-chevron-circle-right"></i>
                                        View this week's payments
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endcan
        </div>
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        jQuery(function ($) {
            window.LoanApplication.this_week_collection_container = '#thisWeekCollectionChartContainer';
            window.LoanApplication.monthly_collection_container = '#monthlyCollectionChart';
            window.LoanApplication.get_remote_data();

        });
    </script>
@endpush
