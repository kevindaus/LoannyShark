@extends('layouts.app', ['activePage' => 'loan_payment', 'titlePage' => __('Loan Payment')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">
                                {{ env('APP_NAME')  }} Loan Payment
                            </h4>
                            <p class="card-category">
                                Loan Payment History Record
                            </p>
                        </div>
                        <div class="card-body">
                            <div class="row mt-3">
                                <div class="col-12">
                                    <form action="{{url()->current()}}">
                                        @if($request->has('name'))
                                            Search result : {{$request->get('name')}}
                                        @endif
                                        <div class="input-group mb-3">
                                            <input autofocus type="text" class="form-control" placeholder="Search member (e.g John Doe)" aria-label="Search member (e.g John Doe)" aria-describedby="basic-addon2" name="name">
                                            <div class="input-group-append">
                                                <button class="btn btn-sm btn-outline-secondary" type="button">
                                                    <i class="fa fa-search" style="font-size: 13px"></i>
                                                    Search
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <a href="{{ route('loan_payment.create') }}"
                                       class="btn btn-sm btn-primary">{{ __('Make Payment') }}</a>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table mt-3">
                                    <thead class=" text-primary">
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Contact #
                                    </th>
                                    <th>
                                        Payment
                                    </th>
                                    <th>
                                        Payment Date
                                    </th>
                                    <th class="text-center">
                                        Action
                                    </th>
                                    </thead>
                                    <tbody>
                                    @foreach($loan_payments as $current_loan_payment)
                                        <tr>
                                            <td>
                                                <a href="{{route('member.view',['member'=>$current_loan_payment->loan->member->id])}}">
                                                    {{$current_loan_payment->loan->member->getFullName()}}
                                                </a>
                                            </td>
                                            <td>
                                                {{$current_loan_payment->loan->member->mobile_number}}
                                            </td>
                                            <td class="text-info">
                                                {{
                                                    @peso($current_loan_payment->amount)
                                                }}
                                            </td>
                                            <td class="text-warning">
                                                {{ $current_loan_payment->payment_date->format("F d,Y") }}
                                            </td>
                                            <td class="td-actions text-center">
                                                <a href="{{route('loan_payment.show',['payment'=>$current_loan_payment->id])}}">
                                                    <i class="fa fa-eye"></i> View
                                                </a>
                                                <a href="{{route('loan_payment.print',['payment'=>$current_loan_payment->id])}}">
                                                    <i class="fa fa-print"></i> Print
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
