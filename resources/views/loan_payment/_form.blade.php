<form method="post" action="{{ route('loan_payment.store') }}" autocomplete="off" class="form-horizontal row">
    @csrf
    @method('post')
    @if ($errors->any())
        <div class="col-12">
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Error</strong>
                <br>
                @if ($errors->any())
                    {!! implode('', $errors->all('<div>:message</div>')) !!}
                @endif

            </div>
        </div>
    @endif
    <div class="col-5">
        <legend class="">Member</legend>
        <select class="form-control" id="member_id" name="member_id">
            <option value="" selected="selected">-- Member --</option>
            @foreach($member_with_loan as $current_member_with_loan_balance)
                <option
                    value="{{$current_member_with_loan_balance->member_id}}" {{ ($current_member_with_loan_balance->id ?? old('member_id')) == $current_member_with_loan_balance->id ? "selected='selected'":""  }}>
                    {{ sprintf("%s %s %s",$current_member_with_loan_balance->title , $current_member_with_loan_balance->first_name , $current_member_with_loan_balance->last_name) }}
                </option>
            @endforeach
        </select>
        <div id="member_loan_container" class="mt-5">
            <legend>Loan balance of selected member</legend>
            <select name="loan_id" id="loan_id" class="form-control">
                <option value="" selected="selected">-- List of loan --</option>
            </select>
            @if ($errors->has('loan_id'))
                <span id="name-error" class="error text-danger"
                      for="input-name">{{ $errors->first('loan_id') }}</span>
            @endif
        </div>
        <legend class="mt-5">Amount</legend>
        <div class="form-group {{ $errors->has('amount') ? ' has-danger' : '' }}">
            <input step="0.01" type="number" class="form-control" id="amount" name="amount"
                   value="{{ isset($loan_payment->amount) ?   $loan_payment->amount:old('amount')  }}">
            @if ($errors->has('amount'))
                <span id="name-error" class="error text-danger"
                      for="input-name">{{ $errors->first('amount') }}</span>
            @endif
        </div>
        <legend class="mt-5">Received by</legend>
        <select class="form-control" id="received_by" name="received_by">
            <option value="" selected="selected">-- Received by --</option>
            @foreach($staffs as $current_staff)
                <option
                    value="{{ $current_staff->id  }}" {{ ( $loan_payment->received_by ?? old('received_by') ) == $current_staff->id  ? "selected='selected'":""  }}>
                    {{ sprintf("%s %s %s",$current_staff->title , $current_staff->first_name , $current_staff->last_name) }}
                </option>
            @endforeach
        </select>
        @if ($errors->has('received_by'))
            <span id="name-error" class="error text-danger"
                  for="input-name">{{ $errors->first('received_by') }}</span>
        @endif
    </div>
    <div class="col-4 ml-3">
        <legend>Loan Information</legend>
        <label for="loan_maturity_date">Loan Date</label>
        <input type="text" class="form-control" id="loan_date" name="loan_date" readonly="readonly" value="">
        @if ($errors->has('loan_date'))
            <span id="name-error" class="error text-danger"
                  for="input-name">{{ $errors->first('loan_date') }}</span>
        @endif
        <label for="loan_maturity_date" class="mt-3">Maturity Date</label>
        <input type="text" class="form-control" id="loan_maturity_date" name="maturity_date" readonly="readonly"
               value="">
        @if ($errors->has('loan_maturity_date'))
            <span id="name-error" class="error text-danger"
                  for="input-name">{{ $errors->first('loan_maturity_date') }}</span>
        @endif
        <label for="payment_due_date" class="mt-3">Payment Due Date</label>
        <input type="hidden" class="form-control" id="payment_due_date" name="due_date" readonly="readonly" value="">
        <input type="text" class="form-control" id="payment_due_date_formatted" name="due_date_formatted"
               readonly="readonly" value="">
        @if ($errors->has('due_date'))
            <span id="name-error" class="error text-danger"
                  for="input-name">{{ $errors->first('due_date') }}</span>
        @endif
        <label for="next_payment_due_date" class="mt-3">Next Payment Due Date</label>
        <input type="hidden" class="form-control" id="next_payment_due_date" name="next_due_date" readonly="readonly"
               value="">
        <input type="text" class="form-control" id="next_payment_due_date_formatted" name="next_due_date_formatted"
               readonly="readonly" value="">
        @if ($errors->has('next_payment_due_date_formatted'))
            <span id="name-error" class="error text-danger"
                  for="input-name">{{ $errors->first('next_payment_due_date_formatted') }}</span>
        @endif

        <label for="next_payment_due_date" class="mt-3">Remaining Balance</label>
        <input type="text" class="form-control" id="remaining_balance" name="remaining_balance" readonly="readonly" >

        <input type="hidden" value="{{ \Carbon\Carbon::now()->format("Y-m-d") }}"  name="payment_date">
    </div>
    <div class="col-2">
        <legend>Terms</legend>
        <strong id="number_of_terms">##</strong> <br>
    </div>
    <div class="col-12">
        <button type="submit" class="btn btn-block btn-primary">
            {{ __('Make payment') }}
        </button>
    </div>
</form>
@push('js')
    <script type="text/javascript">
        var loanPaymentForm = new LoanPaymentForm({
            'memberField': "member_id",
            'loan_member': 'loan_id',
            'amount_per_installment': 'amount',
            'loan_date': 'loan_date',
            'maturity_date_field': 'loan_maturity_date',
            'payment_due_date': 'payment_due_date',
            'payment_due_date_formatted': 'payment_due_date_formatted',
            'next_payment_due_date': 'next_payment_due_date',
            'next_payment_due_date_formatted': 'next_payment_due_date_formatted',
            'remaining_balance': 'remaining_balance',
            'number_of_terms':'number_of_terms'
        });
        jQuery("#member_id").change(function (event) {
            let member_id = jQuery(this).val();
            loanPaymentForm.loadMemberLoan(member_id)
        });
    </script>
@endpush
