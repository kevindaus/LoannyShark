@extends('layouts.app', ['activePage' => 'loan', 'titlePage' => __('All Loans')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">
                                {{ env('APP_NAME')  }} Loan Payment
                            </h4>
                            <p class="card-category">
                            </p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <a href="{{ route('loan_payment.index') }}"
                                       class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 " >
                                    <legend class="text-center mt-5">
                                        Payment Transaction Information
                                    </legend>
                                    <table class="table table-bordered table-hover mt-3" style="margin:  0px auto; width: 80%">
                                        <tbody>
                                        <tr>
                                            <td><strong><label for="loan_rate_id" class="bmd-label-static">Loan
                                                        </label></strong></td>
                                            <td>
                                                <a href="{{route('loan.view',['loan_type'=>strtolower($loan_payment->loan->loan_type),'loan'=>$loan_payment->loan->id])}}">
                                                    {{ $loan_payment->loan->loan_type }} | Date Loaned : {{ $loan_payment->loan->date_loaned->format("F d,Y")  }}
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong><label for="date_loaned" class="bmd-label-static">Payment Date</label></strong></td>
                                            <td>{{$loan_payment->payment_date->format("F d,y")}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong><label for="loan_type" class="bmd-label-static">Due Date</label></strong></td>
                                            <td>{{$loan_payment->due_date->format("F d,Y")}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong><label for="amount_loaned" class="bmd-label-static">Amount</label></strong></td>
                                            <td>{{ @peso($loan_payment->amount) }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong><label for="maturity_date" class="bmd-label-static"> Received By </label></strong></td>
                                            <td>
                                                {{$loan_payment->received_by_staff()->fullName()}}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
