@extends('layouts.app', ['activePage' => 'loan_payment', 'titlePage' => __('Make loan payment')])
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-info" role="alert">
                            {{ session()->get('success')  }}
                        </div>
                    @endif

                    <div class="card ">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">{{ __('Member Information') }}</h4>
                            <p class="card-category"></p>
                        </div>
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-12">
                                    <a href="{{ route('loan_payment.index') }}"
                                       class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                                </div>
                            </div>
                            <br>
                            @include('loan_payment._form',compact('member_with_loan'))
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
