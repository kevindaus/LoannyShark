<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Member;
use Faker\Generator as Faker;

$factory->define(Member::class, function (Faker $faker) {
    return [
        'title' => $faker->titleMale,
        'first_name' => $faker->firstNameMale,
        'last_name' => $faker->lastName,
        'middle_name' => $faker->lastName,
        'suffix' => $faker->suffix,
        'religion' => 'Catholic',
        'date_of_birth' => $faker->dateTimeThisCentury->format('Y-m-d'),
        'occupation' => $faker->jobTitle,
        'monthly_income' => $faker->numberBetween(1000,100000),
        'other_source_of_income' =>'none',
        'civil_status' => 'married',
        'spouse_name' => sprintf("%s %s %s" , $faker->titleFemale , $faker->firstNameFemale,$faker->lastName),
        'spouse_address' => $faker->address,
        'spouse_occupation' => $faker->jobTitle,
        'spouse_monthly_income' => $faker->numberBetween(1000,100000),
        'elementary_school_graduated' => $faker->address,
        'highschool_graduated' => $faker->address,
        'college_graduated' => $faker->address,
        'height' => $faker->numberBetween(80,120),
        'weight' => $faker->numberBetween(20,50),
        'birthplace' => $faker->address,
        'number_of_dependents' => $faker->numberBetween(0,5),
        'address' => $faker->address,
        'business_address' => $faker->address,
        'telephone_number' => $faker->phoneNumber,
        'mobile_number' => $faker->phoneNumber,
        'branch' => $faker->word,
        'date_joined' => date("Y-m-d"),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
