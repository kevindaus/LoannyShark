<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ApplicationSetting;
use Faker\Generator as Faker;

$factory->define(ApplicationSetting::class, function (Faker $faker) {
    return [
        'setting_name' => $faker->word,
        'setting_value' => $faker->word,
        'setting_description' => $faker->paragraph,
        'created_at' => $faker->date('Y-m-d'),
        'updated_at' => $faker->date('Y-m-d')
    ];
});
