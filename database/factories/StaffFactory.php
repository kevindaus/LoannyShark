<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Staff;
use Faker\Generator as Faker;

$factory->define(Staff::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'first_name' => $faker->name,
        'last_name' => $faker->lastName,
        'middle_name' => $faker->lastName,
        'suffix' => $faker->suffix,
        'position' => 'Manager',
        'religion' => 'Catholic',
        'date_of_birth' => $faker->dateTimeThisCentury->format('Y-m-d'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')

    ];
});
