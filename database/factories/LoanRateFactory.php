<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\LoanRate;
use Faker\Generator as Faker;

$factory->define(LoanRate::class, function (Faker $faker) {
    return [
        'name'=>$faker->word,
        'interest_rate' => $faker->numberBetween(1, 10),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
