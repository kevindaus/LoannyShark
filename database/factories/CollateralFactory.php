<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Collateral;
use App\Member;
use Faker\Generator as Faker;

$factory->define(Collateral::class, function (Faker $faker) {
    return [
        'member_id'=>factory(Member::class)->create()->id,
        'name' => $faker->word,
        'type' => $faker->word,
        'description' => $faker->word,
        'picture' => $faker->word,
        'created_at' => $faker->date('Y-m-d'),
        'updated_at' => $faker->date('Y-m-d')
    ];
});
