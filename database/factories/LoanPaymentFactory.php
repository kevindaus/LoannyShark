<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Loan;
use App\LoanPayment;
use Faker\Generator as Faker;

$factory->define(LoanPayment::class, function (Faker $faker) {
    $paymentDate = \Carbon\Carbon::now();
    $dueDate = clone $paymentDate;
    $dueDate->addDays(5);

    return [
        'loan_id'=> factory(Loan::class)->create()->id,
        'due_date'=>$dueDate->format('Y-m-d'),
        'payment_date'=>$paymentDate->format('Y-m-d'),
        'amount'=>$faker->numberBetween(100,1500),
        'received_by'=> factory('App\Staff')->create()->id
    ];
});
