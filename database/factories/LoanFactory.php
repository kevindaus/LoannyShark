<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Loan;
use App\LoanRate;
use App\Member;
use App\Staff;
use Faker\Generator as Faker;

$factory->define(Loan::class, function (Faker $faker) {
    $amount_loaned = $faker->numberBetween(100, 1000);
    $loanDate = \Carbon\Carbon::now();
    $final_loan_date = clone $loanDate;
    $loanDate->addMonths(4);
    $maturity_date = $loanDate->format("Y-m-d");
    $loan_rate = factory(LoanRate::class)->create();
    $loan_rate_id = $loan_rate->id;
    $loan_interest_rate = floatval($loan_rate->interest_rate);
    $amount_to_be_paid = $amount_loaned + ($amount_loaned * ($loan_interest_rate / 100));

    return [
        'member_id' => factory(Member::class)->create()->id,
        'loan_rate_id' => $loan_rate_id,
        'date_loaned' => $final_loan_date->format("Y-m-d"),
        'amount_loaned' => $faker->numberBetween(100, 1000),
        'maturity_date' => $maturity_date,
        'payment_type' => 'installment',
        'amount_to_be_paid' => $amount_to_be_paid,
        'terms' => 4,
        'amount_per_installment' => intval($amount_to_be_paid / 4) ,
        'release_date' => $final_loan_date->format("Y-m-d"),
        'prepared_by' => factory(Staff::class)->create()->id,
        'checked_by' => factory(Staff::class)->create()->id,
        'approved_by' => factory(Staff::class)->create()->id,
        'released_by' => factory(Staff::class)->create()->id,
        'service_charge' => 0,
        'share_capital' => 0,
        'inspection_fee' => 0,
        'notarial' => 0,
        'other' => 0,
        'insurance' => 0,
        'loan_type' => Loan::LOAN_TYPE_COMMODITY,
        'balance' => $amount_to_be_paid,
        'beneficiary' => 0
    ];
});
