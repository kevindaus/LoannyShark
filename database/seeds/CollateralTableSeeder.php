<?php

use App\Collateral;
use App\Loan;
use Illuminate\Database\Seeder;

class CollateralTableSeeder extends Seeder
{
    /**
     * Run the database seeds.nano Loan
     *
     * @return void
     */
    public function run()
    {
        // php artisan db:seed --class="CollateralTableSeeder"
        factory(Collateral::class, 3)->create();
    }
}
