<?php

use App\Member;
use Illuminate\Database\Seeder;

class MemberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // php artisan db:seed --class="MemberTableSeeder"
        factory(Member::class, 30)->create();
        /*@TODO - add collaterol for each member */

        /* add loan on each member*/
    }
}
