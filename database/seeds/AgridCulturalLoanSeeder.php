<?php

use App\Loan;
use Illuminate\Database\Seeder;

class AgridCulturalLoanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // php artisan db:seed --class="AgridCulturalLoanSeeder"
        factory(Loan::class, 10)->create()->each(function($loan){
            $loan->loan_type = Loan::LOAN_TYPE_AGRICULTURAL;
            $loan->save();
        });
    }
}
