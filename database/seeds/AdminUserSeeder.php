<?php

use App\User;
use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // php artisan db:seed --class="AdminUserSeeder"
        factory(User::class, 1)->create()->each(function($user){
            /* @var $user \App\User */
            $user->role = \App\User::ROLE_ADMIN;
            $user->email = 'admin@loan.com';
            $user->save();
        });
    }
}
