<?php

use App\ApplicationSetting;
use App\Staff;
use Illuminate\Database\Seeder;

class ApplicationSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // php artisan db:seed --class="ApplicationSettingsSeeder"
        /* Manager*/
        factory(ApplicationSetting::class, 1)->create()->each(function ($setting) {
            /* @var ApplicationSetting $setting */
            $tempIdContainer = 0;
            factory(Staff::class)->create()->each(function (Staff $staff) use (&$tempIdContainer) {
                $staff->position = \App\User::ROLE_CASHIER;
                $staff->save();
                $tempIdContainer = $staff->id;
            });
            $setting->setting_name = \App\User::ROLE_MANAGER;
            $setting->setting_value = $tempIdContainer;
            $setting->setting_description = \App\User::ROLE_MANAGER;
            $setting->save();
        });
        /*Office Assistant*/
        factory(ApplicationSetting::class, 1)->create()->each(function ($setting) {
            /* @var ApplicationSetting $setting */
            $tempIdContainer = 0;
            $current_staff = factory(Staff::class)->create()->each(function (Staff $staff) use (&$tempIdContainer) {
                $staff->position = \App\User::ROLE_OFFICE_ASSISTANT;
                $staff->save();
                $tempIdContainer = $staff->id;
            });
            $setting->setting_name = \App\User::ROLE_OFFICE_ASSISTANT;
            $setting->setting_value = $tempIdContainer;
            $setting->setting_description = \App\User::ROLE_OFFICE_ASSISTANT;
            $setting->save();
        });
        /* Cashier */
        factory(ApplicationSetting::class, 1)->create()->each(function ($setting) {
            /* @var ApplicationSetting $setting */
            $tempIdContainer = 0;
            $current_staff = factory(Staff::class)->create()->each(function (Staff $staff) use (&$tempIdContainer) {
                $staff->position = \App\User::ROLE_CASHIER;
                $staff->save();
                $tempIdContainer = $staff->id;
            });

            $setting->setting_name = \App\User::ROLE_CASHIER;
            $setting->setting_value = $tempIdContainer;
            $setting->setting_description = \App\User::ROLE_CASHIER;
            $setting->save();
        });
    }
}
