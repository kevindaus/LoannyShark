<?php


use App\LoanRate;
use Illuminate\Database\Seeder;

class LoanRateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // php artisan db:seed --class="LoanRateTableSeeder"
        factory(LoanRate::class, 1)->create()->each(function($loan_rate){
            $loan_rate->name = '1.5 %';
            $loan_rate->interest_rate = 1.5;
            $loan_rate->save();
        });
    }
}
