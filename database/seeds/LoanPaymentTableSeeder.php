<?php


use App\LoanPayment;
use Illuminate\Database\Seeder;

class LoanPaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // php artisan db:seed --class="LoanPaymentTableSeeder"
        factory(LoanPayment::class, 3)->create();
    }
}
