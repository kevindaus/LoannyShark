<?php

use App\User;
use Illuminate\Database\Seeder;

class UserStaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* @var User $currentUser*/
        /*create user for each role */
        // php artisan db:seed --class="UserStaffSeeder"
        factory(User::class, 1)->create()->each(function($currentUser){
            $currentUser->role = User::ROLE_LOAN_OFFICER;
            $currentUser->save();
        });
        factory(User::class, 1)->create()->each(function($currentUser){
            $currentUser->role = User::ROLE_MANAGER;
            $currentUser->save();
        });
        factory(User::class, 1)->create()->each(function($currentUser){
            $currentUser->role = User::ROLE_OFFICE_ASSISTANT;
            $currentUser->save();
        });
        factory(User::class, 1)->create()->each(function($currentUser){
            $currentUser->role = User::ROLE_CASHIER;
            $currentUser->save();
        });
        factory(User::class, 1)->create()->each(function($currentUser){
            $currentUser->role = User::ROLE_ADMIN;
            $currentUser->save();
        });
    }
}
