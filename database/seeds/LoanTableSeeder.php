<?php

use App\Loan;
use App\LoanRate;
use Illuminate\Database\Seeder;

class LoanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // php artisan db:seed --class="LoanTableSeeder"
        factory(Loan::class, 10)->create()->each(function($loan){
            $loan_rate = LoanRate::firstOrCreate([
                'name' => '1.5 %',
                'interest_rate' => 1.5
            ]);
            $loan->loan_rate_id = $loan_rate->id;
            $loan->save();
        });
    }
}
