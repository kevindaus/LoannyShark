<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([UsersTableSeeder::class]);
        $this->call([LoanRateTableSeeder::class]);
        $this->call([CollateralTableSeeder::class]);
        $this->call([LoanPaymentTableSeeder::class]);
        $this->call([LoanTableSeeder::class]);
        $this->call([MemberTableSeeder::class]);
        $this->call([AdminUserSeeder::class]);
        $this->call([ApplicationSettingsSeeder::class]);
        $this->call([UnapprovedLoanSeeder::class]);
    }
}
