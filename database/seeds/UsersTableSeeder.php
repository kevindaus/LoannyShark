<?php


use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // php artisan db:seed --class="UsersTableSeeder"
        factory(User::class, 3)->create();

        /*Manager*/
        factory(User::class, 1)->create()->each(function(User $user){
            $user->email ='manager@gmail.com';
            $user->role = User::ROLE_MANAGER;
            $user->save();
        });
        /*Loan Officer*/
        factory(User::class, 1)->create()->each(function(User $user){
            $user->email ='loan_officer@gmail.com';
            $user->role = User::ROLE_LOAN_OFFICER;
            $user->save();
        });
        /*Account Clerk*/
        factory(User::class, 1)->create()->each(function(User $user){
            $user->email ='account_clerk@gmail.com';
            $user->role = User::ROLE_ACCOUNT_CLERK;
            $user->save();
        });
        /*Cashier*/
        factory(User::class, 1)->create()->each(function(User $user){
            $user->email ='cashier@gmail.com';
            $user->role = User::ROLE_CASHIER;
            $user->save();
        });
        /*Book Keeper*/
        factory(User::class, 1)->create()->each(function(User $user){
            $user->email ='book_keeper@gmail.com';
            $user->role = User::ROLE_BOOK_KEEPER;
            $user->save();
        });
    }
}
