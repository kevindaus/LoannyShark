<?php

use App\Staff;
use App\User;
use Illuminate\Database\Seeder;

class StaffTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // php artisan db:seed --class="StaffTableSeeder"
        factory(Staff::class, 1)->create()->each(function($currentStaff){
            $currentStaff->position = User::ROLE_LOAN_OFFICER;
            $currentStaff->save();
        });
        factory(Staff::class, 1)->create()->each(function($currentUser){
            $currentUser->position = User::ROLE_MANAGER;
            $currentUser->save();
        });
        factory(Staff::class, 1)->create()->each(function($currentUser){
            $currentUser->position = User::ROLE_OFFICE_ASSISTANT;
            $currentUser->save();
        });
        factory(Staff::class, 1)->create()->each(function($currentUser){
            $currentUser->position = User::ROLE_CASHIER;
            $currentUser->save();
        });
        factory(Staff::class, 1)->create()->each(function($currentUser){
            $currentUser->position = User::ROLE_ADMIN;
            $currentUser->save();
        });
    }
}
