<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLoanFeeColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loans', function (Blueprint $table) {
            /* fee */
            $table->double('service_charge')->nullable();
            $table->double('share_capital')->nullable();
            $table->double('inspection_fee')->nullable();
            $table->double('notarial')->nullable();
            $table->double('other')->nullable();
            $table->double('insurance')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loans', function (Blueprint $table) {
            /* fee */
            $table->dropColumn('service_charge');
            $table->dropColumn('share_capital');
            $table->dropColumn('inspection_fee');
            $table->dropColumn('notarial');
            $table->dropColumn('other');
            $table->dropColumn('insurance');
        });

    }
}
