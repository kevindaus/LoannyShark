<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('middle_name');
            $table->string('suffix')->nullable();
            $table->string('religion')->nullable();
            $table->date('date_of_birth')->nullable();

            $table->string('occupation')->nullable();
            $table->string('monthly_income')->nullable();
            $table->string('other_source_of_income')->nullable();

            $table->string('civil_status')->default('single');
            $table->string("spouse_name")->nullable();
            $table->string("spouse_address")->nullable();
            $table->string("spouse_occupation")->nullable();
            $table->string("spouse_monthly_income")->nullable();

            $table->string("elementary_school_graduated")->nullable();
            $table->string("highschool_graduated")->nullable();
            $table->string("college_graduated")->nullable();
            $table->string("height")->nullable();
            $table->string("weight")->nullable();
            $table->string("birthplace")->nullable();
            $table->unsignedInteger('number_of_dependents')->default(0);

            $table->string("address");
            $table->string("business_address")->nullable();

            $table->string("telephone_number")->nullable();
            $table->string("mobile_number")->nullable();

            $table->string("branch");
            $table->date("date_joined");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
