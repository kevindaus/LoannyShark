<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('member_id')->unsigned()->nullable();
            $table->bigInteger('loan_rate_id')->unsigned();

            $table->date('date_loaned');
            $table->double('amount_loaned');
            $table->date('maturity_date');
            $table->string('payment_type');
            $table->double('amount_to_be_paid'); //total amount to be paid
            $table->integer('terms');// how many times the payment will be done , number of times of payment
            $table->double('amount_per_installment');
            $table->double('balance')->default(0);//remaining balance

            $table->date('release_date');
            $table->foreign('member_id')
                ->references('id')
                ->on('members')
                ->onDelete(DB::raw('set null'));

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan');
    }
}
