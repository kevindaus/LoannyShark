<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnAtLoanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loans', function (Blueprint $table) {
            $table->bigInteger('prepared_by')->unsigned()->nullable();
            $table->bigInteger('checked_by')->unsigned()->nullable();
            $table->bigInteger('approved_by')->unsigned()->nullable();

            $table->foreign('prepared_by')->references('id')->on('staffs')->onDelete('set null');
            $table->foreign('checked_by')->references('id')->on('staffs')->onDelete('set null');
            $table->foreign('approved_by')->references('id')->on('staffs')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
