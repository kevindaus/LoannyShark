<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => true,
    'verify' => false,
]);

Route::get('/', 'HomeController@index')->name('home')->middleware('auth');

Route::prefix('dashboard')->name('dashboard.')->middleware(['middleware' => 'auth'])->group(function () {
    Route::get('/', 'DashboardController@index')->name('index');
    Route::get('settings', 'DashboardController@settings')->name('settings');
    Route::post("settings", "DashboardController@update_settings")->name('update_settings');
});
Route::prefix('api')->name('api.')->middleware('auth')->group(function () {
    Route::get('members', 'MemberController@api')->name('members');
    Route::get('member/{member}/loans', 'MemberController@loans')->name('loans');
});

Route::prefix('members')->name('member.')->middleware(['auth'])->group(function () {
    Route::get('/', 'MemberController@index')->name('index');
    Route::get('deactivated', 'MemberController@deactivated_members')->name('deactivated_members');
    Route::get('view/{member}', 'MemberController@show')->name('view');
    Route::get('update/{member}', 'MemberController@update')->name('update');
    Route::get('register', 'MemberController@create')->name('register');
    Route::post('store', 'MemberController@store')->name('store');
    Route::get('deactivate/{member}', 'MemberController@deactivationForm')->name('deactivation_form');
    Route::post('deactivate/{member}', 'MemberController@deactivate')->name('deactivate');
    Route::get('reactivate/{member}', 'MemberController@reactivate')->name('reactivate');
    Route::post('delete/{member}', 'MemberController@destroy')->name('delete');
});
Route::prefix('collateral')->middleware(['auth'])->name('collateral.')->group(function () {
    Route::get('all', 'CollateralController@index')->name('all');
    Route::get('view', 'CollateralController@show')->name('show');
    Route::get('add', 'CollateralController@create')->name('add');
    Route::post('add', 'CollateralController@store')->name('store');
    Route::get('{collateral}/edit', 'CollateralController@edit')->name('edit');
    Route::post('{collateral}/edit', 'CollateralController@update')->name('update');
    Route::delete('{collateral}/delete', 'CollateralController@destroy')->name('destroy');
});


Route::prefix('staff')->name('staff.')->middleware(['middleware' => 'auth'])->group(function () {
    Route::get('/', 'StaffController@index')->name('index');
    Route::get('create', 'StaffController@create')->name('create');
    Route::get('view/{staff}', 'StaffController@show')->name('show');
    Route::post('store', 'StaffController@store')->name('store');
    Route::get('update/{staff}', 'StaffController@update')->name('update');
    Route::patch('update/{staff}', 'StaffController@store')->name('patch');
    Route::delete('destroy/{staff}', 'StaffController@destroy')->name('destroy');
});

Route::prefix('loan')->name('loan.')->middleware(['middleware' => 'auth'])->group(function () {
    Route::get('/', 'LoanController@index')->name('index');

    Route::get('unappoved', 'LoanController@unapproved')->name('unapproved');
    Route::get('approve/{loan}', 'LoanController@approve')->name('approve');

    Route::get('release', 'LoanController@release')->name('release');
    Route::post('release', 'LoanController@release_loan')->name('release_loan');

    /*show loan form - with prefilled loan type*/
    Route::get('register/{loan_type}', 'LoanController@create')->name('register');
    Route::post('register/{loan_type}', 'LoanController@store')->name('store');

    /* edit loan form - with prefilled loan type*/
    Route::get('update/{loan_type}/{loan}', 'LoanController@edit')->name('update');
    Route::post('update/{loan_type}/{loan}}', 'LoanController@store')->name('loan_update');

    /*view whole loan information */
    Route::get('view/{loan_type}/{loan}', 'LoanController@view')->name('view');
    /*delete*/
    Route::get('destroy/{loan_type}/{loan}', 'LoanController@destroy')->name('delete');
    Route::get('print/{loan_type}/{loan}', 'LoanController@print')->name('print');

    Route::get('suspend/{loan}', 'LoanController@loanSuspensionForm')->name('loan_suspension_form');
    Route::post('suspend/{loan}', 'LoanController@suspend')->name('suspend');
    Route::post('delete/{loan}', 'LoanController@destroy')->name('delete');

});
Route::prefix('loan-payment')->name('loan_payment.')->middleware(['middleware' => 'auth'])->group(function () {
    Route::get('/', 'LoanPaymentController@index')->name('index');
    Route::get('show/{payment}', 'LoanPaymentController@show')->name('show');
    Route::post('delete', 'LoanPaymentController@destroy')->name('destroy');
    Route::get('print/{payment}', 'LoanPaymentController@print')->name('print');

    Route::get('create', 'LoanPaymentController@create')->name('create');
    Route::get('create/{loan}', 'LoanPaymentController@create')->name('make_loan_payment');
    Route::post('create', 'LoanPaymentController@store')->name('store');
    Route::get('today', 'LoanPaymentController@today')->name('today');
    Route::get('week', 'LoanPaymentController@week')->name('week');
    Route::get('month', 'LoanPaymentController@month')->name('month');
    Route::get('year', 'LoanPaymentController@year')->name('year');
});

Route::prefix('dashboard')->name('dashboard.')->middleware(['middleware' => 'auth'])->group(function () {
    Route::get('/', 'DashboardController@index')->name('index');
    Route::get('settings', 'DashboardController@settings')->name('settings');
});

Route::group(['middleware' => 'auth'], function () {
    /*Delete this soon*/
    Route::get('table-list', function () {
        return view('pages.table_list');
    })->name('table');

    Route::get('typography', function () {
        return view('pages.typography');
    })->name('typography');

    Route::get('icons', function () {
        return view('pages.icons');
    })->name('icons');

    Route::get('map', function () {
        return view('pages.map');
    })->name('map');

    Route::get('notifications', function () {
        return view('pages.notifications');
    })->name('notifications');

    Route::get('rtl-support', function () {
        return view('pages.language');
    })->name('language');

    Route::get('upgrade', function () {
        return view('pages.upgrade');
    })->name('upgrade');
});

Route::group(['middleware' => 'auth'], function () {
    Route::resource('user', 'UserController', ['except' => ['show']]);
    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

