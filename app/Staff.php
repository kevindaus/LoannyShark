<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{

    protected $dates = ['date_of_birth'];
    protected $table = 'staffs';
    protected $guarded = [];
    public function loans()
    {
        return $this->hasMany(Loan::class);
    }

    public function fullName()
    {
        return sprintf("%s %s %s", $this->title, $this->first_name, $this->last_name);
    }
}
