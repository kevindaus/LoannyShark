<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanRate extends Model
{
    public function loan()
    {
        return $this->belongsTo(Loan::class);
    }
}
