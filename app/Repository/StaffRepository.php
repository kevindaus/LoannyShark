<?php


namespace App\Repository;


use App\ApplicationSetting;
use App\Providers\AppServiceProvider;
use App\Staff;

class StaffRepository
{
    /**
     * @return Staff |null
     */
    public function getCashier()
    {
        /* check settings for who is the cashier */
        $temp = ApplicationSetting::where([
            'setting_name' => \App\User::ROLE_CASHIER,
        ])->firstOrFail();
        $staff_id = intval($temp->setting_value);
        $staff = Staff::findOrFail([
            'id'=>$staff_id
        ])->first();
        return $staff;
    }

}
