<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ApplicationSetting
 * @package App
 * @parameter $id
 * @parameter $setting_name
 * @parameter $setting_value
 * @parameter $setting_description
 * @parameter $created_at
 * @parameter $updated_at
 */
class ApplicationSetting extends Model
{
    //
}
