<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class RoleBaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,... $roles)
    {

        if ( in_array(auth()->user()->role,$roles) ) {
            return $next($request);
        }else{
            abort(403,'You are not allowed to access this. Only '.$role.' can access this');
        }


    }
}
