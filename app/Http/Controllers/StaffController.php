<?php

namespace App\Http\Controllers;

use App\Member;
use App\Staff;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class StaffController extends Controller
{
    use RegistersUsers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $staffs = Staff::orderBy('created_at', 'DESC')->paginate(20);
        $positions = [
            User::ROLE_LOAN_OFFICER => "Loan Officer",
            User::ROLE_MANAGER => "Manager",
            User::ROLE_CASHIER => "Cashier",
            User::ROLE_OFFICE_ASSISTANT=> "Office Assistant",
            User::ROLE_MEMBER=> "Member",
            User::ROLE_ADMIN=> "Admin",
        ];

        if ($request->has('name')) {
            $staffs = Staff::orderBy('created_at', 'desc')
                ->orWhere('title', 'like', '%' . $request->get('name') . '%')
                ->orWhere('first_name', 'like', '%' . $request->get('name') . '%')
                ->orWhere('middle_name', 'like', '%' . $request->get('name') . '%')
                ->orWhere('last_name', 'like', '%' . $request->get('name') . '%')
                ->orWhere('position', 'like', '%' . $request->get('name') . '%')
                ->paginate(20);
        }
        return view('staffs.index')->with([
            'staffs' => $staffs,
            'positions' => $positions,
            'request' => $request
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $positions = [
            User::ROLE_LOAN_OFFICER => "Loan Officer",
            User::ROLE_MANAGER => "Manager",
            User::ROLE_CASHIER => "Cashier",
            User::ROLE_OFFICE_ASSISTANT=> "Office Assistant",
            User::ROLE_MEMBER=> "Member",
            User::ROLE_ADMIN=> "Admin",
            User::ROLE_ACCOUNT_CLERK => "Account Clerk",
            User::ROLE_BOOK_KEEPER => "Book Keeper"
        ];
        return view('staffs.create')->with([
            'positions' => $positions
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedStaffData = $request->validate([
            'id' => '',
            'title' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'suffix' => '',
            'date_of_birth' => '',
            'position' => 'required'
        ]);
        $request->request->add([
            'role'=>$validatedStaffData['position'],
            'name'=> sprintf("%s %s %s",$request->get('title') , $request->get('first_name') , $request->get('last_name'))
        ]);
        /* now validate user */
        $validatedUserData = $request->validate([
            'role' => '',
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8']
        ]);

        $validatedStaffData['date_of_birth'] = $validatedStaffData['date_of_birth'] ?? date('Y-m-d');
        $validatedStaffData['date_of_birth'] = Carbon::parse($validatedStaffData['date_of_birth'])->format("Y-m-d");

        if (isset($validatedStaffData['id'])) {
            Staff::updateOrCreate(['id' => @$validatedStaffData['id']], $validatedStaffData);
        } else {
            Staff::create($validatedStaffData);
        }
        /* register the new user */
        $newUser =  User::create([
            'name' => $validatedUserData['name'],
            'email' => $validatedUserData['email'],
            'role' => $validatedUserData['role'],
            'email_verified_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'password' => Hash::make($validatedUserData['password']),
        ]);
        return redirect(url()->previous())->with('success', 'Member information saved!');

    }


    /**
     * Display the specified resource.
     *
     * @param \App\Staff $staff
     * @return \Illuminate\Http\Response
     */
    public function show(Staff $staff)
    {

        return view('staffs.show')
            ->with(['staff' => $staff]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Staff $staff
     * @return \Illuminate\Http\Response
     */
    public function edit(Staff $staff)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Staff $staff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Staff $staff)
    {
        $positions = [
            '' => '-- position --',
            'loan_officer' => 'Loan Officer',
            'manager' => 'Manager',
            'client' => 'Client',
            'cashier' => 'Cashier',
            'office_assistant' => 'Office Assistant',
            'member' => 'Member',
            'admin' => 'Admin',
        ];
        return view('staffs.update')->with([
            'positions' => $positions,
            'staff' => $staff
        ]);
    }


    public function destroy(Staff $staff)
    {
        $staff->delete();
        return redirect(url()->previous())->with('success', 'Staff deleted!');
    }
}
