<?php

namespace App\Http\Controllers;

use App\Repository\StaffRepository;
use PDF;
use App\Loan;
use App\LoanPayment;
use App\LoanRate;
use App\Member;
use App\Staff;
use Collective\Html\HtmlBuilder;
use Facade\FlareClient\Http\Exceptions\NotFound;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use PHPJasper\PHPJasper;

class LoanController extends Controller
{
    private $staff_repository;

    /**
     * LoanController constructor.
     * @param $staff_repository
     */
    public function __construct(StaffRepository $staff_repository)
    {
        $this->staff_repository = $staff_repository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize("view_all_loans", \App\Loan::class);
        $loans = Loan::orderBy('id', 'DESC')->paginate(30);
        if ($request->has('name')) {
            $loans = Loan::orderBy('loans.id', 'DESC')
                ->select('loans.*')
                ->join('members', 'members.id', '=', 'loans.member_id')
                ->orWhere('members.title', 'like', '%' . $request->get('name') . '%')
                ->orWhere('members.first_name', 'like', '%' . $request->get('name') . '%')
                ->orWhere('members.middle_name', 'like', '%' . $request->get('name') . '%')
                ->orWhere('members.last_name', 'like', '%' . $request->get('name') . '%')
                ->paginate(20);
        }
        return view('loan.index')->with([
            'loans' => $loans,
            'request' => $request
        ]);
    }

    public function unapproved()
    {
        $loans = Loan::onlyTrashed()->orderBy('id', 'DESC')->paginate(30);
        return view('loan.unapproved')->with([
            'loans' => $loans
        ]);
    }

    public function approve($loan)
    {
        $loan = intval($loan);
        $loan = Loan::withTrashed()->findOrFail($loan);
        $loan->restore();
        $linkToPrint = sprintf("<a href='%s' class='%s'>%s</a>", route('loan.print', [
            'loan_type' => strtolower($loan->loan_type),
            'loan' => $loan->id
        ]),"click-to-print", 'Click here to print Loan Statement');

        $successMessage = sprintf('Loan approved! %s', $linkToPrint);
        return redirect(route('loan.view', [
            'loan_type' => strtolower($loan->loan_type),
            'loan' => $loan->id,
        ]))->with('success', $successMessage);
    }

    public function view(string $loan_type, Loan $loan)
    {
        $this->authorize('view_loan', $loan);
        if (!in_array($loan_type, ['agricultural', 'contingency', 'commodity', 'multipurpose'])) {
            abort(404);
        }
        $allMembers = Member::get();
        $loan_rates = LoanRate::all();
        $memberList[''] = '-- Select Member --';
        foreach ($allMembers as $currentMember) {
            $memberList[$currentMember->id] = sprintf("%s %s %s", $currentMember->title, $currentMember->first_name, $currentMember->last_name);
        }

        $staffList[''] = '-- Select Staff --';
        $staffs = Staff::get();
        foreach ($staffs as $current_staff) {
            $staffList[$current_staff->id] = sprintf("%s %s %s", $current_staff->title, $current_staff->first_name, $current_staff->last_name);
        }

        return view('loan.view')->with([
            'allMembers' => $memberList,
            'loan_type' => $loan_type,
            'loan_rates' => $loan_rates,
            'staff_list' => $staffList,
            'loan' => $loan
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($loan_type)
    {
        if (!in_array($loan_type, ['agricultural', 'contingency', 'commodity', 'multipurpose'])) {
            abort(404);
        }
        $allMembers = Member::get();
        $loan_rates = LoanRate::all();
        $memberList[''] = '-- Select Member --';
        foreach ($allMembers as $currentMember) {
            $memberList[$currentMember->id] = sprintf("%s %s %s", $currentMember->title, $currentMember->first_name, $currentMember->last_name);
        }

        $staffList[''] = '-- Select Staff --';
        $staffs = Staff::get();
        foreach ($staffs as $current_staff) {
            $staffList[$current_staff->id] = sprintf("%s %s %s", $current_staff->title, $current_staff->first_name, $current_staff->last_name);
        }


        return view('loan.create')->with([
            'allMembers' => $memberList,
            'loan_type' => $loan_type,
            'loan_rates' => $loan_rates,
            'staff_list' => $staffList
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validationRule = [
            'id' => '',
            'member_id' => 'required',
            'loan_rate_id' => 'required',
            'date_loaned' => 'required',
            'loan_type' => 'required',
            'amount_loaned' => 'required|min:0',
            'maturity_date' => 'required',
            'payment_type' => '',//
            'amount_to_be_paid' => 'min:0',//total calculated amount to be paid
            'terms' => 'required', // frequency of payment . 5 times  , 6 times
            'amount_per_installment' => 'required||min:0', // magkano babayaran
            'release_date' => 'required',
            'prepared_by' => 'required', //staff id
            'checked_by' => 'required', //staff id
            'approved_by' => 'required', //staff id
            'service_charge' => '',//fee
            'share_capital' => '',//fee
            'inspection_fee' => '',//fee
            'notarial' => '',//fee
            'other' => '',//fee
            'insurance' => '',
            'beneficiary' => '',//
        ];
        $validatedData = $request->validate($validationRule);
        $release_date = Carbon::createFromFormat("m/d/Y", $validatedData['release_date'])->format("Y-m-d");
        $date_loaned = Carbon::createFromFormat("m/d/Y", $validatedData['date_loaned'])->format("Y-m-d");
        $maturity_date = Carbon::createFromFormat("m/d/Y", $validatedData['maturity_date'])->format("Y-m-d");
        $validatedData['release_date'] = $release_date;
        $validatedData['date_loaned'] = $date_loaned;
        $validatedData['maturity_date'] = $maturity_date;
        if (isset($validatedData['id'])) {
            Loan::updateOrCreate(['id' => @$validatedData['id']], $validatedData);
        } else {
            $validatedData['balance'] = $validatedData['amount_to_be_paid'];
            $newLoanRegistration = Loan::create($validatedData);
            $newLoanRegistration->delete();// wait to be approve
        }
        return redirect(url()->previous())->with('success', 'Loan saved!');

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Loan $loan
     * @return \Illuminate\Http\Response
     */
    public function show(Loan $loan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Loan $loan
     * @return \Illuminate\Http\Response
     */
    public function edit(string $loan_type, Loan $loan)
    {
        return view('loan.update', [
            'loan' => $loan
        ]);
    }


    public function update(string $loan_type, Loan $loan, Request $request)
    {

        return view('loan.update')->with([
            'loan_type' => $loan_type,
            'loan' => $loan,
            'request' => $request,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Loan $loan
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $loan_type, Loan $loan)
    {
        return view('loan.delete');
    }

    public function print($loan_type, Loan $loan)
    {
        $jasper_template = base_path('/resources/jasper_template/Nangalisan Multipurpose Cooperative - Loan.jasper');
        $unique_folder_identifier = sprintf("/public/pdf/%s/%s/%s", $loan_type, date("Y-m-d"), $loan->member->id);
        $output_folder = base_path($unique_folder_identifier);
        if (!file_exists($output_folder)) {
            mkdir($output_folder, 0777, true);
        }
        $current_formatted_date = Carbon::now()->format("F d,Y");
        $name_of_borrower = $loan->member->getFullName();
        $beneficiary = $loan->beneficiary;
        $address = $loan->member->address;//
        $address = str_replace(",", "",$address);
        $address = str_replace("\n", " ", $address);


        $kind_of_loan = strtoupper($loan_type) . ' LOAN';

        $terms = $loan->terms . ' DAY(S)';
        $due_date = $loan->maturity_date->format("F d,Y");
        $amount_of_loan = $loan->amount_loaned;
        $service_charge = $loan->service_charge;
        $share_capital = $loan->share_capital;
        $inspection_fee = $loan->inspection_fee;
        $notarial = $loan->notarial;
        $others = $loan->other;
        $insurance = $loan->insurance;
        $savings_deposit = "0.0";

        $net_proceeds_of_loan =
            number_format(
                floatval($amount_of_loan) +
                floatval($service_charge) +
                floatval($share_capital) +
                floatval($inspection_fee) +
                floatval($notarial) +
                floatval($others),
                2);

        $prepared_by = $loan->prepared_by()->first()->fullName();
        $checked_by = $loan->checked_by()->first()->fullName();
        $approved_by = $loan->checked_by()->first()->fullName();
        $released_by = $this->staff_repository->getCashier()->fullName();

        $total_net_proceeds = $net_proceeds_of_loan;
        $params = [
            'current_formatted_date' => $current_formatted_date,
            'name_of_borrower' => $name_of_borrower,
            'beneficiary' => $beneficiary,
            'address' => $address,
            'kind_of_loan' => $kind_of_loan,
            'terms' => $terms,
            'due_date' => $due_date,
            'amount_of_loan' => sprintf("P %s", $amount_of_loan . '.00'),
            'service_charge' => sprintf("P %s", $service_charge . '.00'),
            'share_capital' => sprintf("P %s", $share_capital . '.00'),
            'inspection_fee' => sprintf("P %s", $inspection_fee . '.00'),
            'notarial' => sprintf("P %s", $notarial . '.00'),
            'others' => sprintf("P %s", $others . '.00'),
            'insurance' => $insurance,
            'savings_deposit' => sprintf("P %s", $savings_deposit . '.00'),
            'net_proceeds_of_loan' => sprintf("P %s", $net_proceeds_of_loan . '.00'),
            'prepared_by' => $prepared_by,
            'checked_by' => $checked_by,
            'approved_by' => $approved_by,
            'released_by' => $released_by,
            'total_net_proceeds' => sprintf("P %s", $total_net_proceeds . '.00')
        ];

        $options = [
            'format' => ['pdf'],
            'params' => $params,
        ];
        $jasper = new PHPJasper;
        $jasper->process(
            $jasper_template,
            $output_folder,
            $options
        )->execute();
        $complete_output_path = asset($unique_folder_identifier) . '/' . basename("$jasper_template", '.jasper') . '.pdf';
        $complete_output_path = str_replace("/public", "", $complete_output_path);


        return redirect($complete_output_path);
    }
}
