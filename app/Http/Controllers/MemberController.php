<?php

namespace App\Http\Controllers;

use App\Loan;
use App\LoanPayment;
use App\Member;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $members = Member::orderBy('created_at', 'desc')->paginate(20);
        if ($request->has('name')) {
            $members = Member::orderBy('created_at', 'desc')
                ->orWhere('title', 'like', '%' . $request->get('name') . '%')
                ->orWhere('first_name', 'like', '%' . $request->get('name') . '%')
                ->orWhere('middle_name', 'like', '%' . $request->get('name') . '%')
                ->orWhere('last_name', 'like', '%' . $request->get('name') . '%')
                ->paginate(20);
        }
        return view('member.index')->with([
            'members' => $members,
            'request'=>$request
        ]);
    }

    public function deactivated_members()
    {
        $members = Member::onlyTrashed()->orderBy('created_at', 'desc')->paginate(20);
        return view('member.deactivated')->with([
            'members' => $members
        ]);
    }

    public function reactivate($member)
    {
        $member = Member::withTrashed()->where(['id' => $member])->firstOrFail();
        $member->restore();

        return redirect(route('member.deactivated_members'))->with(['success' => 'Member approved']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $member = new Member();
        return view('member.create')->with(['member' => $member]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'id' => '',
            'title' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'middle_name' => 'required',
            'branch' => 'required',
            'date_joined' => 'required',
            'date_of_birth' => 'required',
            'suffix' => '',
            'religion' => '',
            'occupation' => '',
            'monthly_income' => '',
            'other_source_of_income' => '',
            'civil_status' => 'required',
            'spouse_name' => '',
            'spouse_address' => '',
            'spouse_occupation' => '',
            'spouse_monthly_income' => '',
            'elementary_school_graduated' => '',
            'highschool_graduated' => '',
            'college_graduated' => '',
            'height' => '',
            'weight' => '',
            'birthplace' => '',
            'number_of_dependents' => '',
            'address' => 'required',
            'business_address' => '',
            'telephone_number' => '',
            'mobile_number' => '',
        ]);
        $formattedDateOfBirth = Carbon::createFromFormat("m/d/Y", $validatedData['date_of_birth'])->format("Y-m-d");
        $dateJoined = Carbon::createFromFormat("m/d/Y", $validatedData['date_joined'])->format("Y-m-d");
        $validatedData['date_of_birth'] = $formattedDateOfBirth;
        $validatedData['date_joined'] = $dateJoined;
        if (isset($validatedData['id'])) {
            Member::updateOrCreate(['id' => @$validatedData['id']], $validatedData);
        } else {
            $newlyCreatedMember = Member::create($validatedData);

            $newlyCreatedMember->delete();
        }

        return redirect(url()->previous())->with('success', 'Member information saved!');
    }


    /**
     * Display the specified resource.
     *
     * @param \App\Member $member
     * @return \Illuminate\Http\Response
     */
    public function show(Member $member)
    {
        return view('member.show')->with([
            'member' => $member
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Member $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        return view('member.update')->with([
            'member' => $member
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Member $member
     * @return \Illuminate\Http\Response
     */
    public function destroy($member)
    {
        $member = Member::onlyTrashed()->where(['id' => intval($member)])->first();
        $member->forceDelete();
        return redirect(route('member.deactivated_members'))->with(['success' => "Member deleted"]);
    }

    public function deactivationForm(Member $member)
    {
        return view('member.delete')
            ->with(['member' => $member]);
    }

    public function deactivate(Member $member)
    {
        $member->delete();
        return redirect(route('member.index'))->with(['success' => "Member deactivated"]);
    }

    public function api()
    {
        return Member::all();
    }

    public function loans(Member $member)
    {
        $member_loans = DB::table('loans')
            ->select('loans.*')
            ->join('members', 'members.id', '=', 'loans.member_id')
            ->where('loans.balance', '>', 0)
            ->where('loans.member_id', '=', $member->id)
            ->get();

        $finalContent = [];
        foreach ($member_loans as $currentMemberLoan) {
            /*maturity date - loan date  = date difference / terms  */
            $maturityDate = Carbon::parse($currentMemberLoan->maturity_date);
            $loanDate = Carbon::parse($currentMemberLoan->date_loaned);
            $currentMemberLoan->formatted_loan_date = $loanDate->format("F d,Y");
            $currentMemberLoan->formatted_maturity_date = $maturityDate->format("F d,Y");
            $daysDiff = $maturityDate->diffInDays($loanDate);
            $paymentIterationFrequency = intval($daysDiff / $currentMemberLoan->terms);

            $paymentDueDates = [];
            $initialPaymentDueDate = $loanDate;
            foreach (range(1, $currentMemberLoan->terms) as $currentIndex) {
                if ($currentIndex === $currentMemberLoan->terms) {
                    $paymentDueDates[] = [
                        'raw' => $maturityDate->format("Y-m-d"),
                        'formatted' => $maturityDate->format("F d,Y"),
                    ];
                } else {
                    $initialPaymentDueDate->format("M d,Y");
                    $initialPaymentDueDate->addDays($paymentIterationFrequency);
                    $paymentDueDates[] = [
                        'raw' => $initialPaymentDueDate->format("Y-m-d"),
                        'formatted' => $initialPaymentDueDate->format("F d,Y"),
                    ];
                }
            }
            /*count all the payments done under this loan record */
            $numberOfPayments = DB::table('loan_payments')->where('loan_id', $currentMemberLoan->id)->count();
            $numberOfPayments = intval($numberOfPayments);
            $currentMemberLoan->payment_due_date = $paymentDueDates[$numberOfPayments];
            $currentMemberLoan->next_payment_due_date = $paymentDueDates[$numberOfPayments + 1] ?? 'none';
            $currentMemberLoan->balance = round($currentMemberLoan->balance, 2);
            $currentMemberLoan->terms = sprintf("%s %s", $currentMemberLoan->terms, "month(s)");

            $finalContent[] = $currentMemberLoan;
        }
        return json_encode($finalContent);
    }
}
