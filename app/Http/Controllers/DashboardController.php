<?php

namespace App\Http\Controllers;

use App\ApplicationSetting;
use App\LoanPayment;
use App\Repository\StaffRepository;
use App\Staff;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    private $staff_reporsitory;

    /**
     * DashboardController constructor.
     * @param StaffRepository $staff_repository
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function __construct(StaffRepository $staff_repository)
    {
        $this->staff_reporsitory = $staff_repository;

    }


    public function index(Request $request)
    {
        $this->authorize('view_dashboard', auth()->user());
        return view('dashboard.index')->with([
            'request' => $request
        ]);
    }

    /**
     * View all settings
     */
    public function settings()
    {
        $all_staffs = Staff::all();
        $cashier = $this->staff_reporsitory->getCashier();
        $allStaffsArr = [];
        $allStaffsArr[''] = '-- Please select a Staff --';
        foreach ($all_staffs as $current_staff) {
            $allStaffsArr[$current_staff->id] = $current_staff->fullName();
        }

        return view('dashboard.settings')->with([
            'all_staffs' => $allStaffsArr,
            'cashier' => $cashier
        ]);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update_settings(Request $request)
    {
        $validatedData = $request->validate([
            'cashier' => 'required'
        ]);
        ApplicationSetting::where([
            'setting_name' => 'cashier',
        ])
            ->update([
                'setting_name' => 'cashier',
                'setting_value' => $validatedData['cashier']
            ]);
        return redirect(url()->previous())->with([
            'success' => "Settings saved!",
        ]);
    }
}
