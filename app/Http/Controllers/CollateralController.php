<?php

namespace App\Http\Controllers;

use App\Collateral;
use App\Loan;
use App\Member;
use Illuminate\Http\Request;

class CollateralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $collaterals = Collateral::orderBy("id",'desc')->paginate(20);
        if ($request->has('name')) {
            $collaterals = Collateral::orderBy('collaterals.id','DESC')
                ->join('members','members.id','=','collaterals.member_id')
                ->orWhere('members.title', 'like', '%' . $request->get('name') . '%')
                ->orWhere('members.first_name', 'like', '%' . $request->get('name') . '%')
                ->orWhere('members.middle_name', 'like', '%' . $request->get('name') . '%')
                ->orWhere('members.last_name', 'like', '%' . $request->get('name') . '%')
                ->paginate(20);
        }

        return view('collateral.index')->with([
            'collaterals' => $collaterals,
            'request' => $request
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $members = Member::all();
        return view('collateral.add')->with([
            'members' => $members
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'member_id' => 'required',
            'name' => 'required',
            'type' => 'required',
            'description' => '',
            'picture' => '',
        ]);
        Collateral::create($validatedData);
        return redirect(url()->previous())->with('success', 'Collateral information saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Collateral $collateral
     * @return \Illuminate\Http\Response
     */
    public function show(Collateral $collateral)
    {
        return view('collateral.show')->with([
            'collateral' => $collateral
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Collateral $collateral
     * @return \Illuminate\Http\Response
     */
    public function edit(Collateral $collateral)
    {
        $members = Member::order('id','DESC')->all();
        return view('collateral.edit')->with([
            'collateral' => $collateral,
            'members' => $members
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Collateral $collateral
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Collateral $collateral)
    {
        $validatedData = $request->validate([
            'member_id' => 'required',
            'name' => 'required',
            'type' => 'required',
            'description' => '',
        ]);
        $collateral->member_id = $validatedData['member_id'];
        $collateral->name = $validatedData['name'];
        $collateral->type = $validatedData['type'];
        $collateral->description = $validatedData['description'];
        $collateral->save();
        return redirect(url()->previous())->with('success', 'Collateral information saved!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Collateral $collateral
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Collateral $collateral)
    {
        $collateral->delete();
        return redirect(url()->previous())->with('success', 'Collateral removed!');
    }
}
