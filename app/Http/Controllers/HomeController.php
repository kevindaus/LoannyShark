<?php

namespace App\Http\Controllers;

use App\Loan;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view_dashboard', auth()->user());
        $loans = Loan::orderBy('id', 'DESC')->paginate(30);
        if ($request->has('name')) {
            $loans = Loan::orderBy('loans.id', 'DESC')
                ->select('loans.*')
                ->join('members', 'members.id', '=', 'loans.member_id')
                ->orWhere('members.title', 'like', '%' . $request->get('name') . '%')
                ->orWhere('members.first_name', 'like', '%' . $request->get('name') . '%')
                ->orWhere('members.middle_name', 'like', '%' . $request->get('name') . '%')
                ->orWhere('members.last_name', 'like', '%' . $request->get('name') . '%')
                ->paginate(20);
        }
        return view('dashboard.index')->with([
            'loans'=>$loans
        ]);
    }
}



