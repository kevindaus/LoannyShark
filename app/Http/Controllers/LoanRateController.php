<?php

namespace App\Http\Controllers;

use App\LoanRate;
use Illuminate\Http\Request;

class LoanRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoanRate  $loanRate
     * @return \Illuminate\Http\Response
     */
    public function show(LoanRate $loanRate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoanRate  $loanRate
     * @return \Illuminate\Http\Response
     */
    public function edit(LoanRate $loanRate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoanRate  $loanRate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoanRate $loanRate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoanRate  $loanRate
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoanRate $loanRate)
    {
        //
    }
}
