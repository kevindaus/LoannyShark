<?php

namespace App\Http\Controllers;

use PDF;
use App\Loan;
use App\LoanPayment;
use App\Member;
use App\Staff;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class LoanPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $loan_payments = LoanPayment::orderBy('created_at', 'DESC')->get();
        if ($request->has('name')) {
            $loan_payments = LoanPayment::orderBy('loans.id', 'DESC')
                ->join('loans', 'loans.id', 'loan_payments.loan_id')
                ->join('members', 'members.id', '=', 'loans.member_id')
                ->orWhere('members.title', 'like', '%' . $request->get('name') . '%')
                ->orWhere('members.first_name', 'like', '%' . $request->get('name') . '%')
                ->orWhere('members.middle_name', 'like', '%' . $request->get('name') . '%')
                ->orWhere('members.last_name', 'like', '%' . $request->get('name') . '%')
                ->paginate(20);
        }
        return view('loan_payment.index')->with([
            'loan_payments' => $loan_payments,
            'request' => $request
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /* find member with unpaid loan */
        $member_with_loan = DB::table('members')
            ->join('loans', 'members.id', '=', 'loans.member_id')
            ->where('loans.balance', '>', 0)
            ->get();

        $staffs = Staff::all();

        return view('loan_payment.create')->with([
            'member_with_loan' => $member_with_loan,
            'staffs' => $staffs
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'id' => '',
            'loan_id' => 'required',
            'due_date' => 'required',
            'payment_date' => 'required',
            'amount' => 'required',
            'received_by' => 'required'
        ]);

        if (isset($validatedData['id'])) {
            LoanPayment::updateOrCreate(['id' => @$validatedData['id']], $validatedData);
        } else {
            $loanPayment = LoanPayment::create($validatedData);
            /* reduce loan balance */

            $loanPayment->loan->balance = $loanPayment->loan->balance - $loanPayment->amount;
            $loanPayment->loan->save();
        }
        return redirect(url()->previous())->with('success', 'Loan payment accepted!');

    }

    public function show(LoanPayment $payment)
    {

        return view('loan_payment.show')->with([
            'loan_payment' => $payment
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\LoanPayment $loanPayment
     * @return \Illuminate\Http\Response
     */
    public function edit(LoanPayment $loanPayment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\LoanPayment $loanPayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoanPayment $loanPayment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\LoanPayment $loanPayment
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoanPayment $loanPayment)
    {
        dd($loanPayment);

    }

    public function today()
    {
        echo __METHOD__;
    }

    public function week()
    {
        /* get dates of this week */
        /*loop */
        $result['monday'] = date("Y-m-d", strtotime("this week monday"));
        $result['tuesday'] = date("Y-m-d", strtotime("this week tuesday"));
        $result['wednesday'] = date("Y-m-d", strtotime("this week wednesday"));
        $result['thursday'] = date("Y-m-d", strtotime("this week thursday"));
        $result['friday'] = date("Y-m-d", strtotime("this week friday"));
        $result['saturday'] = date("Y-m-d", strtotime("this week saturday"));
        $result['sunday'] = date("Y-m-d", strtotime("this week sunday"));
        foreach ($result as $weekDayName => $currentWeekDay) {
            /* search and count*/
            $result[$weekDayName] = $this->getWeekPaymentCount($currentWeekDay);
        }
        return $result;
    }

    private function getWeekPaymentCount(string $payment_date)
    {
        $result = DB::table('loan_payments')
            ->select(DB::raw('SUM(amount) as total_payment'))
            ->where('payment_date', '=', $payment_date)
            ->get();
        return floatval($result[0]->total_payment);
    }

    public function month()
    {
        $report = [];
        /* get dates of this week */
        foreach (range(1, 12) as $currentMonthNum) {
            $result = DB::table('loan_payments')
                ->select(DB::raw('SUM(amount) as total_payment'), DB::raw("month(payment_date) as payment_month_date"), DB::raw("monthname(payment_date) as monthname"))
                ->groupBy(['payment_month_date', 'monthname'])
                ->having('payment_month_date', '=', $currentMonthNum)
                ->get();
            $report[$currentMonthNum] = $result[0] ?? ['total_payment' => 0, 'payment_month_date' => $currentMonthNum, 'monthname' => date("F", strtotime("1991-$currentMonthNum-30"))];
        }

        return $report;
    }

    public function print(LoanPayment $payment)
    {
//        $pdf = PDF::loadView('print.loan_payment', $payment);
//        return $pdf->stream();
        return view('print')->with([
            'payment'=>$payment
        ]);
    }

}
