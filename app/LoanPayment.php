<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LoanPayment
 * @package App
 */
class LoanPayment extends Model
{
    protected $dates = ['payment_date','due_date'];
    protected $guarded = [];

    public function loan()
    {
        return $this->belongsTo(Loan::class);
    }

    public function received_by_staff()
    {
        return Staff::where([
            'id'=>$this->received_by
        ])->firstOrFail();
    }


}
