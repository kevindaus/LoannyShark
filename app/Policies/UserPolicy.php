<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;
    /**
     * @var array
     */
    private $allowed_roles_for_dashboard;

    /**
     * UserPolicy constructor.
     */
    public function __construct()
    {

        $this->allowed_roles_for_dashboard = [
//            User::ROLE_LOAN_OFFICER,
            User::ROLE_MANAGER,
//            User::ROLE_CLIENT,
//            User::ROLE_CASHIER,
//            User::ROLE_OFFICE_ASSISTANT,
//            User::ROLE_MEMBER,
            User::ROLE_ADMIN
        ];
    }


    /**
     * Determine whether the user can view any models.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param \App\User $user
     * @param \App\User $model
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function register_new_user(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param \App\User $user
     * @param \App\User $model
     * @return mixed
     */
    public function update(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\User $user
     * @param \App\User $model
     * @return mixed
     */
    public function delete(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param \App\User $user
     * @param \App\User $model
     * @return mixed
     */
    public function restore(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param \App\User $user
     * @param \App\User $model
     * @return mixed
     */
    public function forceDelete(User $user, User $model)
    {
        //
    }

    public function view_dashboard(User $user)
    {
        return in_array($user->role, [
            User::ROLE_MANAGER,
//            User::ROLE_ACCOUNT_CLERK,
            User::ROLE_ADMIN
        ]);
    }

    public function view_member(User $user)
    {
        return in_array($user->role, [
            User::ROLE_MANAGER,
            User::ROLE_LOAN_OFFICER,
            User::ROLE_ADMIN
        ]);
    }


    public function view_collateral(User $user)
    {
        return in_array($user->role, [
            User::ROLE_MANAGER,
//            User::ROLE_ACCOUNT_CLERK,
            User::ROLE_ADMIN
        ]);
    }

    public function view_payment_history(User $user)
    {
        return in_array($user->role, [
            User::ROLE_MANAGER,
            User::ROLE_ACCOUNT_CLERK,
            User::ROLE_LOAN_OFFICER,
            User::ROLE_ADMIN
        ]);
    }

    public function view_users(User $user)
    {
        return in_array($user->role, [
            User::ROLE_MANAGER,
            User::ROLE_ADMIN
        ]);
    }

    public function view_settings(User $user)
    {
        return in_array($user->role, [
            User::ROLE_MANAGER,
            User::ROLE_ADMIN
        ]);
    }

    public function view_staff(User $user)
    {
        return in_array($user->role, [
            User::ROLE_MANAGER,
            User::ROLE_ACCOUNT_CLERK,
            User::ROLE_ADMIN
        ]);
    }



}
