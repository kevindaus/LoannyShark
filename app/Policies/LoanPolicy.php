<?php

namespace App\Policies;

use App\User;
use App\Loan;
use Illuminate\Auth\Access\HandlesAuthorization;

class LoanPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any loans.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the loan.
     *
     * @param \App\User $user
     * @param \App\Loan $loan
     * @return mixed
     */
    public function view(User $user, Loan $loan)
    {
        //
    }

    /**
     * Determine whether the user can create loans.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the loan.
     *
     * @param \App\User $user
     * @param \App\Loan $loan
     * @return mixed
     */
    public function update(User $user, Loan $loan)
    {
        //
    }

    /**
     * Determine whether the user can delete the loan.
     *
     * @param \App\User $user
     * @param \App\Loan $loan
     * @return mixed
     */
    public function delete(User $user, Loan $loan)
    {
        //
    }

    /**
     * Determine whether the user can restore the loan.
     *
     * @param \App\User $user
     * @param \App\Loan $loan
     * @return mixed
     */
    public function restore(User $user, Loan $loan)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the loan.
     *
     * @param \App\User $user
     * @param \App\Loan $loan
     * @return mixed
     */
    public function forceDelete(User $user, Loan $loan)
    {
        //
    }

    public function view_all_loans(User $user)
    {

        return in_array($user->role, [
            User::ROLE_MANAGER,
            User::ROLE_CASHIER,
            User::ROLE_LOAN_OFFICER,
            User::ROLE_ACCOUNT_CLERK,
            User::ROLE_BOOK_KEEPER,
            User::ROLE_ADMIN
        ]);
    }

    public function view_loan(User $user, Loan $loan)
    {

        return in_array($user->role, [
            User::ROLE_MANAGER,
            User::ROLE_CASHIER,
            User::ROLE_LOAN_OFFICER,
            User::ROLE_ACCOUNT_CLERK,
            User::ROLE_ADMIN
        ]);
    }

    public function update_loan(User $user, Loan $loan)
    {
        return in_array($user->role, [
            User::ROLE_MANAGER,
            User::ROLE_ACCOUNT_CLERK,
            User::ROLE_ADMIN
        ]);
    }

    public function print_loan(User $user, Loan $loan)
    {

        return in_array($user->role, [
            User::ROLE_MANAGER,
            User::ROLE_CASHIER,
            User::ROLE_LOAN_OFFICER,
            User::ROLE_ACCOUNT_CLERK,
            User::ROLE_BOOK_KEEPER,
            User::ROLE_ADMIN
        ]);
    }

    public function view_pending_loan(User $user)
    {
        return in_array($user->role, [
            User::ROLE_MANAGER,
            User::ROLE_ACCOUNT_CLERK,
            User::ROLE_ADMIN
        ]);
    }

    public function make_loan(User $user, Loan $loan)
    {
        return in_array($user->role, [
            User::ROLE_MANAGER,
            User::ROLE_ADMIN
        ]);
    }
}
