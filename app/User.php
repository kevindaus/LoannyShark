<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class User
 * @package App
 * @property $username;
 * @property $password;
 * @property $role
 */
class User extends Authenticatable
{
    use Notifiable;
    const ROLE_MANAGER = 'manager';
    const ROLE_LOAN_OFFICER = 'loan_officer';
    const ROLE_ACCOUNT_CLERK = 'account_clerk';
    const ROLE_CASHIER = 'cashier';
    const ROLE_BOOK_KEEPER = 'book_keeper';


    const ROLE_OFFICE_ASSISTANT = 'office_assistant';
    const ROLE_MEMBER = 'member';
    const ROLE_ADMIN = 'admin';


    protected $attributes = [
        'role' => User::ROLE_MEMBER
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'role', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


}
