<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Loan
 * @package App
 * @property  Member $member
 * @property $id
 * @property $member_id
 * @property $loan_rate_id
 * @property $date_loaned
 * @property $amount_loaned
 * @property $maturity_date
 * @property $payment_type
 * @property $amount_to_be_paid
 * @property $terms
 * @property $amount_per_installment
 * @property $balance
 * @property $release_date
 * @property $created_at
 * @property $updated_at
 * @property Staff $prepared_by
 * @property Staff $checked_by
 * @property Staff $approved_by
 * @property Staff $released_by
 * @property $service_charge
 * @property $share_capital
 * @property $inspection_fee
 * @property $notarial
 * @property $other
 * @property $insurance
 * @property $beneficiary
 * @property $loan_type
 * @property $deleted_at
 */
class Loan extends Model
{
    use SoftDeletes;
    protected $dates = ['date_loaned', 'maturity_date', 'release_date', 'created_at', 'updated_at'];
    protected $attributes = [
        'payment_type' => 'installment'
    ];
    protected $guarded = [];
    const LOAN_TYPE_AGRICULTURAL = 'AGRICULTURAL';
    const LOAN_TYPE_CONTINGENCY = 'CONTINGENCY';
    const LOAN_TYPE_COMMODITY = 'COMMODITY';
    const LOAN_TYPE_MULTIPURPOSE = 'MULTIPURPOSE';

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function loan_rate()
    {
        return $this->belongsTo(LoanRate::class);
    }


    public function loan_payments()
    {
        return $this->hasMany(LoanPayment::class);
    }


    public function prepared_by()
    {
        return $this->hasOne(Staff::class, 'id', 'prepared_by');
    }

    public function checked_by()
    {
        return $this->hasOne(Staff::class, 'id', 'checked_by');
    }

    public function approved_by()
    {
        return $this->hasOne(Staff::class, 'id', 'approved_by');
    }

    public function released_by()
    {
        return $this->hasOne(Staff::class, 'id', 'released_by');
    }

}
