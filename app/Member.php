<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Member
 * @package App
 * @property $id
 * @property $title
 * @property $first_name
 * @property $last_name
 * @property $middle_name
 * @property $suffix
 * @property $religion
 * @property $date_of_birth
 * @property $occupation
 * @property $monthly_income
 * @property $other_source_of_income
 * @property $civil_status
 * @property $spouse_name
 * @property $spouse_address
 * @property $spouse_occupation
 * @property $spouse_monthly_income
 * @property $elementary_school_graduated
 * @property $highschool_graduated
 * @property $college_graduated
 * @property $height
 * @property $weight
 * @property $birthplace
 * @property $number_of_dependents
 * @property $address
 * @property $business_address
 * @property $telephone_number
 * @property $mobile_number
 * @property $branch
 * @property $date_joined
 * @property $created_at
 * @property $updated_at
 * @property $deleted_at
 */
class Member extends Model
{
    use SoftDeletes;

    protected $dates = ['date_of_birth', 'date_joined', 'created_at', 'updated_at'];
    protected $guarded = [];
    protected $attributes = [
        'branch' => 'Bagabag Branch',
        'number_of_dependents' => 0
    ];

    public function collateral()
    {
        return $this->hasMany(Collateral::class);
    }


    public function loan()
    {
        return $this->hasMany(Loan::class);
    }

    public function getFullName()
    {
        return sprintf('%s %s %s', $this->title, $this->first_name, $this->last_name);
    }
}
